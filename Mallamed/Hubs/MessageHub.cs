﻿using System.Collections.Generic;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace Mallamed.Hubs
{
    [HubName("messageHub")]
    public class MessageHub : Hub
    {
        [HubMethodName("sendMessages")]
        public static void SendMessages()
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<MessageHub>();
            context.Clients.All.updateMessages();
        }

    }

    [HubName("notifiyHub")]
    public class NotifiyHub : Hub
    {
        [HubMethodName("sendNotify")]
        public static void SendNotify()
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<NotifiyHub>();
            context.Clients.All.updateNotify();
           // context.Clients.Users(Empid).updateNotify();
        }


    }
}