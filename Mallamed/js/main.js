/*global $, jQuery*/
$(document).ready(function () {
  'use strict';
  
    $('.navbar-expand-lg .navbar-nav .dropdown-toggle').click(function () {
        $(this).siblings('.navbar-expand-lg .navbar-nav .dropdown-menu').slideToggle();
    });

  
//  Social static bar
  $('.social-bar .facebook button').click(function () {
    
    $(this).css('borderRight', 'none');
    $('.social-bar .twitter button, .social-bar .instagram button').css('borderRight', '3px solid #4267B2');
    $('.social-bar .facebook').css('zIndex', '115');
    $('.social-bar .twitter').css('zIndex', '114');
    $('.social-bar .instagram').css('zIndex', '113');
    
    if($('.social-bar').hasClass('slide') &&  $('.social-bar .facebook').hasClass('active')) {
      $('.social-bar').toggleClass('slide');
      $('.social-bar').css('borderColor', '#4267B2');
    } else {
      $('.social-bar .facebook').addClass('active').siblings().removeClass('active');
      $('.social-bar').addClass('slide');
      $('.social-bar').css('borderColor', '#4267B2');
    }
    
  });
  
  $('.social-bar .twitter button').click(function () {
    
    $(this).css('borderRight', 'none');
    $('.social-bar .facebook button, .social-bar .instagram button').css('borderRight', '3px solid #1DA1F2');
    $('.social-bar .twitter').css('zIndex', '115');
    $('.social-bar .facebook, .social-bar .instagram').css('zIndex', '114');
    
    if($('.social-bar').hasClass('slide') &&  $('.social-bar .twitter').hasClass('active')) {
      $('.social-bar').toggleClass('slide');
      $('.social-bar').css('borderColor', '#1DA1F2');
    } else {
      $('.social-bar .twitter').addClass('active').siblings().removeClass('active');
      $('.social-bar').addClass('slide');
      $('.social-bar').css('borderColor', '#1DA1F2');
    }
    
  });
  
  $('.social-bar .instagram button').click(function () {
    
    $(this).css('borderRight', 'none');
    $('.social-bar .facebook button, .social-bar .twitter button').css('borderRight', '3px solid #E13E60');
    $('.social-bar .instagram').css('zIndex', '115');
    $('.social-bar .facebook').css('zIndex', '113');
    $('.social-bar .twitter').css('zIndex', '114');
    
    if($('.social-bar').hasClass('slide') &&  $('.social-bar .instagram').hasClass('active')) {
      $('.social-bar').toggleClass('slide');
      $('.social-bar').css('borderColor', '#E13E60');
    } else {
      $('.social-bar .instagram').addClass('active').siblings().removeClass('active');
      $('.social-bar').addClass('slide');
      $('.social-bar').css('borderColor', '#E13E60');
    }
    
  });
  


    $('.testimonialCarosal').slick({
        dots: true,
        infinite: true,
        speed: 300,
        autoplay: true,
        slidesToShow: 1,
        adaptiveHeight: true
    });


    $('.NewPartner').slick({
        infinite: true,
        autoplay: true,
        speed: 300,
        slidesToShow:4,
        slidesToScroll: 1,
        responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            arrows: false
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            arrows: false
          }
        },
        {
          breakpoint: 500,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false
          }
        }
      ]
    });

    $('.TemporaryDeals').slick({
        infinite: true,
        speed: 300,
        autoplay: true,
        slidesToShow:4,
        slidesToScroll: 1,
        responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            arrows: false
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            arrows: false
          }
        },
        {
          breakpoint: 500,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false
          }
        }
      ]
    });
    $(function(){
      $(".MoreItems").click(function(e){
        $(this).toggleClass("hide");
        $(".MorePartners").toggleClass("open");

        e.preventDefault();

      });
    });
    $(function(){
      $(".MoreCategory").click(function(e){
        $(this).toggleClass("hide");
        $(".MoreCategories").toggleClass("open");

        e.preventDefault();

      });
    });
    $(function(){
      $(".MoreTours").click(function(e){
        $(this).toggleClass("hide");
        $(".MoreTourisme").toggleClass("open");

        e.preventDefault();

      });
    });
 $('.ourGallery').slick({
      centerMode: true,
      centerPadding: '60px',
      slidesToShow: 3,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 3
          }
        },
        {
          breakpoint: 480,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 1
          }
        }
      ]
    });
    lightbox.option({
      'resizeDuration': 100,
      'albumLabel': ''
    })
});

jssor_1_slider_init = function () {

    var jssor_1_SlideshowTransitions = [
        { $Duration: 800, x: 0.3, $During: { $Left: [0.3, 0.7] }, $Easing: { $Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
        { $Duration: 800, x: -0.3, $SlideOut: true, $Easing: { $Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
        { $Duration: 800, x: -0.3, $During: { $Left: [0.3, 0.7] }, $Easing: { $Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
        { $Duration: 800, x: 0.3, $SlideOut: true, $Easing: { $Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
        { $Duration: 800, y: 0.3, $During: { $Top: [0.3, 0.7] }, $Easing: { $Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
        { $Duration: 800, y: -0.3, $SlideOut: true, $Easing: { $Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
        { $Duration: 800, y: -0.3, $During: { $Top: [0.3, 0.7] }, $Easing: { $Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
        { $Duration: 800, y: 0.3, $SlideOut: true, $Easing: { $Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
        { $Duration: 800, x: 0.3, $Cols: 2, $During: { $Left: [0.3, 0.7] }, $ChessMode: { $Column: 3 }, $Easing: { $Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
        { $Duration: 800, x: 0.3, $Cols: 2, $SlideOut: true, $ChessMode: { $Column: 3 }, $Easing: { $Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
        { $Duration: 800, y: 0.3, $Rows: 2, $During: { $Top: [0.3, 0.7] }, $ChessMode: { $Row: 12 }, $Easing: { $Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
        { $Duration: 800, y: 0.3, $Rows: 2, $SlideOut: true, $ChessMode: { $Row: 12 }, $Easing: { $Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
        { $Duration: 800, y: 0.3, $Cols: 2, $During: { $Top: [0.3, 0.7] }, $ChessMode: { $Column: 12 }, $Easing: { $Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
        { $Duration: 800, y: -0.3, $Cols: 2, $SlideOut: true, $ChessMode: { $Column: 12 }, $Easing: { $Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
        { $Duration: 800, x: 0.3, $Rows: 2, $During: { $Left: [0.3, 0.7] }, $ChessMode: { $Row: 3 }, $Easing: { $Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
        { $Duration: 800, x: -0.3, $Rows: 2, $SlideOut: true, $ChessMode: { $Row: 3 }, $Easing: { $Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
        { $Duration: 800, x: 0.3, y: 0.3, $Cols: 2, $Rows: 2, $During: { $Left: [0.3, 0.7], $Top: [0.3, 0.7] }, $ChessMode: { $Column: 3, $Row: 12 }, $Easing: { $Left: $Jease$.$InCubic, $Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
        { $Duration: 800, x: 0.3, y: 0.3, $Cols: 2, $Rows: 2, $During: { $Left: [0.3, 0.7], $Top: [0.3, 0.7] }, $SlideOut: true, $ChessMode: { $Column: 3, $Row: 12 }, $Easing: { $Left: $Jease$.$InCubic, $Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
        { $Duration: 800, $Delay: 20, $Clip: 3, $Assembly: 260, $Easing: { $Clip: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
        { $Duration: 800, $Delay: 20, $Clip: 3, $SlideOut: true, $Assembly: 260, $Easing: { $Clip: $Jease$.$OutCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
        { $Duration: 800, $Delay: 20, $Clip: 12, $Assembly: 260, $Easing: { $Clip: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
        { $Duration: 800, $Delay: 20, $Clip: 12, $SlideOut: true, $Assembly: 260, $Easing: { $Clip: $Jease$.$OutCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 }
    ];

    var jssor_1_options = {
        $AutoPlay: 1,
        $SlideshowOptions: {
            $Class: $JssorSlideshowRunner$,
            $Transitions: jssor_1_SlideshowTransitions,
            $TransitionsOrder: 1
        },
        $ArrowNavigatorOptions: {
            $Class: $JssorArrowNavigator$
        },
        $ThumbnailNavigatorOptions: {
            $Class: $JssorThumbnailNavigator$
        }
    };

    var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

    /*#region responsive code begin*/

    var MAX_WIDTH = 980;

    function ScaleSlider() {
        var containerElement = jssor_1_slider.$Elmt.parentNode;
        var containerWidth = containerElement.clientWidth;

        if (containerWidth) {

            var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

            jssor_1_slider.$ScaleWidth(expectedWidth);
        }
        else {
            window.setTimeout(ScaleSlider, 30);
        }
    }

    ScaleSlider();

    $Jssor$.$AddEvent(window, "load", ScaleSlider);
    $Jssor$.$AddEvent(window, "resize", ScaleSlider);
    $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
    /*#endregion responsive code end*/
};

jssor_1_slider_init();