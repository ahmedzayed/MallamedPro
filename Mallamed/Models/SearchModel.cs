﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mallamed.Entity;

namespace Mallamed.Models
{
    public class SearchModel
    {

        public SearchModel()
        {

        }
        public SearchModel(Landmark c)
        {
            Id = c.Id;
            type = "معلم";
            url = "/Home/LandMarkDetails?id="+c.Id;
            Photo = c.PhotoPath;
            name = c.Name;
            details = c.Details;
        }
        public SearchModel(Event c)
        {
            Id = c.Id;
            type = "فاعلية";
            url = "/Home/EventDetails?id=" + c.Id;
            Photo = c.PhotoPath;
            name = c.Name;
            details = c.Details;
        }
        public SearchModel(Donator c)
        {
            Id = c.ID;
            type = "تجاري";
            url = "/Services/Details?id=" + c.ID;
            Photo = c.Photo;
            name = c.OrgName;
            details = c.TradeActivity;
        }

        public int Id { get; set; }
        public string type { get; set; }
        public string url { get; set; }
        public string Photo { get; set; }
        public string name { get; set; }
        public string details { get; set; }
    }
}