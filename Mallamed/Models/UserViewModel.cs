﻿using System.ComponentModel.DataAnnotations;

namespace Mallamed.Models
{
    public class UserLogInModel
    {
        [Required(ErrorMessage = "الايميل مطلوب")]
        [EmailAddress(ErrorMessage = "الايميل غير صحيح")]
        public string Email { get; set; }

        [Required(ErrorMessage = "الباسورد مطلوب")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }

    public class UserViewModel
    {
        [Required(ErrorMessage = "الاسم بالكامل مطلوب")]
        public string FullName { get; set; }

        public string UserName { get; set; }

        [Required(ErrorMessage = "الهاتف مطلوب")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "الايميل مطلوب")]
        [EmailAddress(ErrorMessage = "الايميل غير صحيح")]
        public string Email { get; set; }

        [Required(ErrorMessage = "الباسورد مطلوب")]
        [StringLength(100, ErrorMessage = "كلمه السر ضعيفه يجب ان تحتوي علي حرف كابتل وحروف مثل @#$ وارقام")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "تاكيد الباسورد مطلوب")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "كلمه السر غير متطابقه")]
        public string ConfirmPassword { get; set; }
    }
}