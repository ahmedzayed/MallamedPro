﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Mallamed.Models
{
    public class RegisterAsDonatorModel
    {

        [Required(ErrorMessage = "قم بادخال تاريخ بداية العقد")]
        [DataType(DataType.DateTime)]
        public DateTime? ContractDate { get; set; }

        [Required(ErrorMessage = "قم بادخال اسم المنظمة")]
        [MaxLength(150, ErrorMessage = "اسم المنظمة لا يجب ان يتجاوز 150 حرف")]
        public string OrgName { get; set; }

        [Required(ErrorMessage = "قم باختيار القسم")]
        public int? CatID { get; set; }

        [Required(ErrorMessage = "قم بادخال تاريخ الاصدار")]
        [DataType(DataType.DateTime)]
        public DateTime? Versiondate { get; set; }

        [Required(ErrorMessage = "قم بادخال اسم المالك")]
        [MaxLength(150, ErrorMessage = "اسم المالك لا يجب ان يتجاوز 150 حرف")]
        public string OwnName { get; set; }

        [Required(ErrorMessage = "قم بادخال اسم المسؤل")]
        [MaxLength(150, ErrorMessage = "اسم المسؤل لا يجب ان يتجاوز 150 حرف")]
        public string ResponsableName { get; set; }

        [Required(ErrorMessage = "قم بادخال العنوان")]
        [MaxLength(250, ErrorMessage = "العنوان لا يجب ان يتجاوز 250 حرف")]
        public string Address { get; set; }

        [Required(ErrorMessage = "قم بادخال رقم السجل")]
        [MaxLength(150, ErrorMessage = "رقم السجل لا يجب ان يتجاوز 150 حرف")]
        public string BookName { get; set; }

        [Required(ErrorMessage = "قم بادخال الايميل")]
        [MaxLength(150, ErrorMessage = "الايميل لا يجب ان يتجاوز 150 حرف")]
        [DataType(DataType.EmailAddress, ErrorMessage = "بريد الكتروني غير صالح")]
        public string Email { get; set; }

        public string TradeActivity { get; set; }

        [Required(ErrorMessage = "قم بادخال تاريخ انتهاء العقد")]
        [DataType(DataType.DateTime)]
        public DateTime? finishDate { get; set; }

        [MaxLength(50, ErrorMessage = "رقم الجوال 1 لا يجب ان يتجاوز 50 حرف")]
        public string Phone1 { get; set; }

        [MaxLength(50, ErrorMessage = "رقم الجوال 2 لا يجب ان يتجاوز 50 حرف")]
        public string Phone2 { get; set; }

        [MaxLength(50, ErrorMessage = "التليفون  لا يجب ان يتجاوز 50 حرف")]
        public string Telephone { get; set; }

        [Required(ErrorMessage = "قم بادخال تاريخ االزياره ")]
        [DataType(DataType.DateTime)]
        public DateTime? VisiteDate { get; set; }

        [Required(ErrorMessage = "قم بادخال تاريخ الخصم ")]
        [DataType(DataType.DateTime)]
        public DateTime? DiscountDate { get; set; }

        [Required(ErrorMessage = "قم باختيار المنطقه")]
        public int? RegionId { get; set; }
    }
}