﻿using System.Configuration;
using Mallamed.Hubs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Mallamed.Entity;
using Mallamed.Models;

namespace Mallamed.Models
{
    public class MessagesRepository
    {
        private readonly string _connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        MallamDbFEntities db =new MallamDbFEntities();
        public IEnumerable<ChatRoom> GetAllMessages(int sender, int reciver)
        {
            var messages = new List<ChatRoom>();
            using (var connection = new SqlConnection(_connString))
            {
                connection.Open();
                using (var command = new SqlCommand(@"SELECT [chat_id],[id_recieve],[id_send],[message],[time] FROM [dbo].[chating] WHERE (id_recieve=" + reciver + " AND id_send=" + sender + ") OR (id_recieve=" + sender + " AND id_send=" + reciver + ")", connection))
                {
                    command.Notification = null;

                    var dependency = new SqlDependency(command);
                    dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                       // messages.Add(new ChatRoom { chat_id = (int)reader["chat_id"], id_recieve = (int)reader["id_recieve"], id_send = (int)reader["id_send"], message = (string)reader["message"], time = (DateTime)reader["time"] });
                    }
                }

            }
            return messages;
        }

        public IEnumerable<DemondChat> GetAllNotifications()
        {
            var demond = new List<DemondChat>();
            using (var connection = new SqlConnection(_connString))
            {
                connection.Open();
                using (var command = new SqlCommand(@"SELECT [ID],[UserType],[UserID],[CreateDate] FROM [dbo].[DemondChat] WHERE (Status=0)", connection))
                {
                    command.Notification = null;

                    var dependency = new SqlDependency(command);
                    dependency.OnChange += new OnChangeEventHandler(dependency_OnChangeNotify);

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        try
                        {
                            demond.Add(new DemondChat
                            {
                                ID = (int)reader[0],
                                UserType = reader[1].ToString(),
                                UserID = (int)reader[2],
                                CreateDate = (DateTime?)reader[3] ?? DateTime.Now
                            });
                        }
                        catch (Exception ){}
                    
                    }
                }

            }
            return demond;
        }

        private void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                MessageHub.SendMessages();
            }
        }
        private void dependency_OnChangeNotify(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                              NotifiyHub.SendNotify();
            }
        }

    }
}