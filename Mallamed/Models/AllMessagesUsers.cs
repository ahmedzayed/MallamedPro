﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mallamed.Models
{
    public class AllMessagesUsers
    {
        public int IdAnother { get; set; }
        public string NameAnother { get; set; }
        public string Message { get; set; }
        public string gender { get; set; }
        public DateTime Time { get; set; }
        public string ImageAnother { get; set; }

    }
}