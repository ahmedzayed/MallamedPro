﻿using Mallamed.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mallamed.Models
{
    public class HomePage
    {
        private MallamDbFEntities db = new MallamDbFEntities();
        public HomePage()
        {
            LandmarkTypes = db.LandmarkTypes.ToList();
        }
        public Ws_Cards MainCard { get; set; }
        public List<Ws_Cards> Cards { get; set; }
        public Ws_Video Video { get; set; }
        public List<Ws_Photos>  photos { get; set; }
        public List<Ws_Slider> Sliders { get; set; }
        public List<Donator> Donators { get; set; }
        public List<LandmarkType> LandmarkTypes { get; set; }
        public List<Event> Events { get; set; }
        public List<WebsiteStatistic> WebsiteStatistics { get; set; }
    }
    public class Cardss
    {
        public Ws_Cards MainCard { get; set; }
        public List<Ws_Cards> Cards { get; set; }


    }
}