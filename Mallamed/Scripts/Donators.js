﻿function GetDonators(id) {
    event.preventDefault();
    var liAnchor = $('#' + id);
    $.get(liAnchor.attr("href").trim(), {}, function (data) {
        $("#divGetDonators").html(data);
        $(".GetLies li").each(function () { $(this).removeClass("active"); });
        liAnchor.closest("li").addClass("active");
    });
}
$(document).ready(function () {
    var url = window.location.href;
    var Id = url.includes("/") ? url.substring(url.lastIndexOf("/") + 1, url.lastIndexOf("?")).trim() : "";
    if (Id != "") { $("#c" + Id).closest("li").addClass("active").siblings().removeClass("active"); }
});