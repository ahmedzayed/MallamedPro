﻿using Mallamed.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
namespace Mallamed.Controllers
{
    public class ServicesController : Controller
    {
        MallamDbFEntities db = new MallamDbFEntities();
        public ActionResult Index()
        {
            ViewBag.name = "الاقسام";
            var cates = db.Categorys.ToList();
            return View(cates);
        }


        public ActionResult Categorys()
        {
            var cates = db.Categorys.ToList();
            return PartialView(cates);
        }


        public ActionResult Regions()
        {
            var Regions = db.Regions.ToList();
            return PartialView(Regions);
        }
        public ActionResult Donators(int? id,string type,int?page)
        {
            ViewBag.id = id;
            ViewBag.type = type;
            var donators = new List<Donator>();
            if (id == null)
            {
                return RedirectToAction("Index");
            }
                ViewBag.name = "الخدمات الدائمة والمؤقتة";
            if (type == "r") 
                donators = db.Donators.Where(c => c.RegionId == id).Where(c => c.IsActive == 1 || c.IsActive == 2).ToList();
            else
                donators = db.Donators.Where(c=>c.CatID==id).Where(c => c.IsActive == 1 || c.IsActive == 2).ToList();

            return View(donators.ToPagedList(page??1,12));
        }
        public ActionResult Details(int? id)
        {
            if (id != null)
            {
                var don = db.Donators.Find(id);
                if (don != null)
                {
                    return View(don);
                }
            }

            return RedirectToAction("Index");
        }
    }
    public class GetSevices
    {
        public List<Category> Categorys { get; set; }

        public List<Region> Regions { get; set; }
    }
}