﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Mallamed.Entity;
using Mallamed.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Mallamed.ModelView;
using PagedList;
using System.Text.RegularExpressions;
using Mallamed;

namespace Mallamed.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        MallamDbFEntities db=new MallamDbFEntities();
        public HomeController()
        {

        }

        public HomeController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        [HttpGet]
        [AllowAnonymous]
        public ActionResult RegisterAsDonator()
        {
            ViewData["Categories"] = new SelectList(db.Categorys.ToList(), "ID", "Name");
            ViewData["Regions"] = new SelectList(db.Regions.ToList(), "ID", "Name");
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult RegisterAsDonator(Donator donator)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    Regex re = new Regex(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
                    if (re.IsMatch(donator.Email))
                    {
                        db.Donators.Add(donator);
                        db.SaveChanges();
                    }
                    else
                    {
                        return Json("بريد الكتروني غير صالح", JsonRequestBehavior.AllowGet);
                    }
                }
                catch
                {
                    return Json("حدث خطا ما", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("تاكد من كل الحقول اولا", JsonRequestBehavior.AllowGet);
            }

            return Json(1, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ContactUs()
        {
            return View();
        }
        public ActionResult Categorys(int? id, string type)
        {
            ViewBag.Cat = db.Categorys.ToList();
            ViewBag.name = "الاقسام";
            var cates = db.Categorys.ToList();
            if (id != null)
            {
                ViewBag.Donts = id;
            }
            else
            {
                ViewBag.Donts = 2;
            }
            return View(cates);
        }

        public ActionResult AboutUs()
        {
            return View();
        }

        public ActionResult ServeCard()
        {
            return View();
        }

        #region Ajax

        [HttpPost]
        public ActionResult ContactUs(Ws_Contactus Contactus)
        {
            
            db.Ws_Contactus.Add(Contactus);
            db.SaveChanges();
            TempData["msg"] = "<script>alert(' تم ارسال الرسالة بنجاح');</script>";

            return RedirectToAction("ContactUs");
        }

        //[HttpPost]
        //public JsonResult ServeCard(ServeCard serveCard)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        if (UserManager.FindByEmail(serveCard.Email) == null)
        //        {
        //            if (UserManager.FindByName(serveCard.Username) == null)
        //            {
        //                var crd = new Card() { Email = serveCard.Email, Name = serveCard.Name, Phone = serveCard.Phone };
        //                db.Cards.Add(crd);
        //                if (db.SaveChanges() > 0)
        //                {
        //                    AspNetUser user = new AspNetUser()
        //                    {
        //                        UserName = serveCard.Username,
        //                        Email = serveCard.Email,
        //                        Type = 4,
        //                        FullName = serveCard.Name
        //                    };
        //                    var suc = UserManager.Create(user, serveCard.Password);
        //                    if (suc.Succeeded)
        //                    {
        //                        UserManager.AddToRole(user.Id, "Client");
        //                        SignInManager.PasswordSignIn(user.UserName, serveCard.Password, false, false);
        //                    }
        //                    else
        //                    {
        //                        db.Cards.Remove(crd);
        //                        db.SaveChanges();
        //                    }
        //                }
        //                else
        //                {

        //                }
        //                return Json(2, JsonRequestBehavior.AllowGet);
        //            }
        //            else
        //            {
        //                return Json(new { id = 1, msg = " اسم المستخدم موجود بالفعل" }, JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        else
        //        {
        //            return Json(new { id = 0, msg = "البريد الالكتروني موجود بالفعل" }, JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //    return Json(-1, JsonRequestBehavior.AllowGet);
        //}

        #endregion

        #region Partial Views

        public PartialViewResult Categories()
        {
            return PartialView("_Categories", db.Categorys.ToList());
        }

        public PartialViewResult Events()
        {
            return PartialView("_Events", db.Events.Where(x => x.EndDate > DateTime.Now).OrderByDescending(x => x.EndDate).Take(2).ToList());
        }

        public PartialViewResult LatestDonators()
        {
            return PartialView("_LatestDonators", db.Donators.OrderByDescending(x => x.ID).Where(x => x.IsActive == 1 || x.IsActive == 2).Take(8).ToList());
        }

        public PartialViewResult Slider()
        {
            return PartialView("_Slider", db.Ws_Slider.Where(x => x.IsActive).ToList());
        }

        public PartialViewResult LandMark()
        {
            return PartialView("_LandMark", db.Landmarks.ToList());
        }

        public PartialViewResult EventDonatorCateCounts()
        {
            List<int?> Counts = new List<int?>()
            {
                db.Landmarks.Count(),
                db.Events.Count() ,
                db.Donators.Count()
            };
            return PartialView("_EventDonatorCateCounts", Counts);
        }

        public PartialViewResult News()
        {
            return PartialView("_News", db.News.OrderByDescending(x => x.Date).Take(3).ToList());
        }

        public PartialViewResult TemporaryServices()
        {
            return PartialView("_TemporaryServices", db.DonatedServices.Where(x => x.EndDate >= DateTime.Now).ToList());
        }

        public PartialViewResult OurPartners()
        {
            return PartialView("_OurPartners", db.Ws_GoldSponsors.ToList());
        }

        #endregion
        //public ActionResult Index()
        //{
        //    var homes = new HomePage()
        //    {
        //        Cards = db.Ws_Cards.Where(c => c.Main == false).ToList(),
        //        MainCard = db.Ws_Cards.FirstOrDefault(c => c.Main),
        //        Video = db.Ws_Video.FirstOrDefault(c => c.Isactive),
        //        photos = db.Ws_Photos.Where(c => c.Isactive).ToList(),
        //        WebsiteStatistics = db.WebsiteStatistics.ToList(),
        //        Donators = db.Donators.ToList(),
        //        LandmarkTypes=db.LandmarkTypes.ToList(),
        //        Events=db.Events.OrderByDescending(c=>c.Id).Take(6).ToList(),
        //        Sliders =db.Ws_Slider.Where(c=>c.IsActive).ToList()
        //    };
        //    return View(homes);
        //}
        //[HttpGet]
        //public ActionResult Contactus()
        //{
        //    return View();
        //}
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Contactus(Ws_Contactus contact)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Ws_Contactus.Add(contact);
        //        db.SaveChanges();
        //        ModelState.AddModelError("", "تم ارسال الرسالة بنجاح");
        //    }
        //    else
        //    {
        //        ModelState.AddModelError("", "حدث خطا  يرجي ادخال كل البايانات");
        //    }
        //    return View();
        //}

        public ActionResult Search(string search)
        {
            var events = db.Events.Where(c=>c.Name.Contains(search)).ToList().Select(c => new SearchModel(c));
            var landmark = db.Landmarks.Where(c=>c.Name.Contains(search)).ToList().Select(c => new SearchModel(c));
            var donator = db.Donators.Where(c=>c.OrgName.Contains(search)&&(c.IsActive==1||c.IsActive==2)).ToList().Select(c => new SearchModel(c));
            var list = new List<SearchModel>();
            list.AddRange(events.ToList());
            list.AddRange(landmark);
            list.AddRange(donator);
            return View(list);
        }

        public ActionResult AlbomPhoto()
        {
            var items = db.Ws_Photos.Where(c=>c.Isactive==true).ToList();
            return View(items);
        }

        public ActionResult contactinfo(int id)
        {
            ViewBag.id = id;
            var contact = db.WebSiteContactInfoes.FirstOrDefault();
            if (contact == null)
                contact = new WebSiteContactInfo();
            return PartialView(contact);
        }

        public ActionResult Socialmedia()
        {
            var so = db.Ws_SocialMedia.Where(c=>c.Isactive).ToList();
            return PartialView(so);
        }
        public ActionResult SocialmediaFooter()
        {
            var so = db.Ws_SocialMedia.Where(c => c.Isactive).ToList();
            return PartialView(so);
        }
        //public ActionResult LandMark(int? id,int ?page)
        //{
        //    if (id != null)
        //    {
        //        ViewBag.id = id;
        //        try { ViewBag.name = db.LandmarkTypes.Find(id).Name;} catch { }
        //    }
        //    return View(db.Landmarks.Where(c=>id==null||c.LandmarkId==id).OrderBy(c=>c.Id).ToPagedList(page??1,12));
        //}
        public ActionResult LandMarkDetails(int id)
        {
            return View(db.Landmarks.Find(id));
        }
        public ActionResult getLandMarkTypes()
        {
            var items = db.LandmarkTypes.ToList();
            return PartialView(items);
        }
        public ActionResult GetEventType()
        {
            var items = db.EventTypes.ToList();
            return PartialView(items);
        }
        [HttpGet]
        public ActionResult DemondCard()
        {
            var card = new Cardss()
            {
                Cards = db.Ws_Cards.Where(c => c.Main == false).ToList(),
                MainCard = db.Ws_Cards.FirstOrDefault(c => c.Main)
            };
            return View(card);
        }
        [HttpPost]
        public ActionResult DemondCard(DemondCard card)
        {
            if (ModelState.IsValid)
            {
                if (UserManager.FindByEmail(card.Email) == null && UserManager.FindByName(card.Username) == null)
                {
                    var cards = new Card {Name = card.Name, Email = card.Email, Phone = card.Phone};
                    db.Cards.Add(cards);
                    var ind = db.SaveChanges();
                    if (ind > 0)
                    {
                        var usr = new ApplicationUser
                        {
                            UserName = card.Username,
                            Email = card.Email,
                            Type = 4,
                            FullName = card.Name,
                            UserID = cards.ClientID
                        };
                      var suc=  UserManager.Create(usr, card.Password);
                        if (!suc.Succeeded)
                        {
                            db.Cards.Remove(cards);
                            db.SaveChanges();
                            ModelState.AddModelError("", "يرجي اخال الكلمة المرورو اكبر من 6 احرف");
                        }
                        else
                        {
                            var user = UserManager.FindByName(usr.UserName);
                            UserManager.AddToRole(user.Id, "Client");
                            SignInManager.PasswordSignIn(user.UserName, card.Password, false, false);
                            return RedirectToAction("Index", "Clients/ClientControlPanel");
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("","البريد الاليكتروني او اسم المستخدم متواجد بالفعل يرجي تغييرهم");
                }
            
               
            }

            return View(card);
        }

        public ActionResult Event(int? Id , int? page)
        {
            if (Id != null)
            {
                ViewBag.id = Id;
                try { ViewBag.name = db.EventTypes.Find(Id).Name; } catch { }
            }
            return View(db.Events.Where(c => Id == null || c.EventTypeId == Id).OrderBy(c => c.Id).ToPagedList(page ?? 1, 12));
        }
        public ActionResult EventDetails(int? id)
        {
            var Event = db.Events.Find(id);
            //if (id != 0)
            //{
            //    ViewBag.EventPhoto = db.EventPhotoes.Where(a => a.EventId == id).ToList();
            //    ViewBag.EventResposor = db.EventSponsors.Where(a => a.EventId == id).ToList();
            //    ViewBag.EventSoc = db.EventSocialMedias.Where(a => a.EventId == id).ToList();
            //}
            //else
            //{
            //    ViewBag.EventPhoto = db.EventPhotoes.ToList();
            //    ViewBag.EventResposor = db.EventSponsors.ToList();
            //    ViewBag.EventSoc = db.EventSocialMedias.ToList();
            //}
            return View(Event);
        }
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult AboutCard()
        {

            return View(db.Ws_Cards.ToList());

        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {

            var user = UserManager.FindByName(model.Username);

            if (user != null)
            {
                var result = SignInManager.PasswordSignIn(model.Username, model.Password, model.RememberMe, shouldLockout: false);
                switch (result)
                {
                    case SignInStatus.Success:
                        {
                            await SignInAsync(user, model.RememberMe);

                            if (UserManager.IsInRole(user.Id, "Admin"))
                            {
                                return RedirectToAction("Index", "Admin/AdminHome");
                            }
                            if (UserManager.IsInRole(user.Id, "Employee"))
                            {
                                return RedirectToAction("Index", "Employee/Empcpanel");
                            }
                            if (UserManager.IsInRole(user.Id, "Donator"))
                            {
                                return RedirectToAction("Index", "Donators/cpanel");
                            }
                            if (UserManager.IsInRole(user.Id, "Client"))
                            {
                                return RedirectToAction("Index", "Clients/ClientControlPanel");
                            }

                            return RedirectToAction("Login", "Account");
                        }
                    default:
                        return View(model);
                }
            }

            ModelState.AddModelError("", "خطا في كلمه السر او الايميل");
            return RedirectToAction("Login","Account",model);
            
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }



        #region Helpers

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }


        #endregion


    }
}