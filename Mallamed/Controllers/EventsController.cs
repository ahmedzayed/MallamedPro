﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Helpers;
using Core.Viewmodels.Admin;
using Mallamed.Entity;


namespace Mallamed.Controllers
{
    public class EventsController : Controller
    {
        private readonly MallamDbFEntities db = new MallamDbFEntities();

        public ActionResult Index()
        {
            return View(db.EventTypes.ToList());
        }


        public ActionResult Details(int? Id)
        {
            return View(db.Events.Where(x => x.Id == Id).FirstOrDefault());
        }
        public ActionResult EventByTypeId(int? EventTypeId, int? RegionId)
        {
            ViewBag.EventTYpeId = EventTypeId;
            ViewBag.RegionId = RegionId;


            return View();


        }
        public ActionResult EventList(int EventTypeId, int? RegionId)
        {
            if (RegionId == null)
            {
                return View(db.Events.Where(x => x.EndDate > DateTime.Now).OrderByDescending(x => x.EndDate).ToList());
            }
            else
            {
                return View(db.Events.Where(x => x.EndDate > DateTime.Now && x.RegionId==RegionId).OrderByDescending(x => x.EndDate).ToList());

            }
        }
        public ActionResult GetRegion(int Id, int RegionId = 0)
        {
            var RegionList = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            RegionList.AddRange("Sp_RegionDropBycity".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<RegionDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.ID.ToString(),
                Selected = s.ID == RegionId ? true : false
            }).ToList());
            ViewBag.RegionId = RegionList;
            return PartialView("GetRegionByCity");
        }
        public ActionResult RegionList(int? EventTypeId )
        {
            ViewBag.EventTYpeId = EventTypeId;

            var RegionList = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            RegionList.AddRange("Sp_RegionDrop".ExecuParamsSqlOrStored(false).AsList<RegionDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.RegionId = RegionList;
            var lst3 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst3.AddRange("Sp_CityDrop".ExecuParamsSqlOrStored(false).AsList<CityDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.Cities = lst3;
            return PartialView("RegionList");

        }
    }
}