﻿using Mallamed.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Mallamed.Controllers
{
    public class DonatorController : Controller
    {
        private readonly MallamDbFEntities db = new MallamDbFEntities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int? Id)
        {
            return View(db.Donators.Where(x => x.ID == Id).FirstOrDefault());
        }

        #region Partial Views

        public PartialViewResult Categories()
        {
            var model = db.Categorys.Include("Donators").ToList();
            return PartialView("_Categories",model );
        }

        public PartialViewResult Regions()
        {
            return PartialView("_Regions", db.Regions.Include("Donators").ToList());
        }

        #endregion

        #region Ajax

        public PartialViewResult GetDonators(int? Id, string type)
        {
            if (Id != null)
            {
                List<Donator> Donators = new List<Donator>();

                if (type == "cat")
                {
                    var cat = db.Categorys.Where(x => x.ID == Id).FirstOrDefault();
                    ViewBag.Name = cat.Name;
                    Donators = cat.Donators.Where(x=>x.IsActive == 1 || x.IsActive == 2).ToList();
                }
                else
                {
                    var reg = db.Regions.Where(x => x.ID == Id).FirstOrDefault();
                    if (reg != null)
                    {
                        ViewBag.Name = reg.Name;
                        Donators = reg.Donators.Where(x => x.IsActive == 1 || x.IsActive == 2).ToList();
                    }
                    else
                    {
                        ViewBag.Name = "";
                    }
                }
                return PartialView("_GetDonators", Donators);
            }
            else
            {
                var cat = db.Categorys.FirstOrDefault();
                ViewBag.Name = cat.Name;
                return PartialView("_GetDonators", cat.Donators.Where(x => x.IsActive == 1 || x.IsActive == 2).ToList());
            }
        }

        #endregion

    }
}