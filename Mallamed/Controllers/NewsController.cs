﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mallamed.Entity;

namespace Mallamed.Controllers
{
    public class NewsController : Controller
    {
        private readonly MallamDbFEntities db = new MallamDbFEntities();

        public ActionResult Index()
        {
            var model = db.News.ToList();
            return View(model);
        }

        public ActionResult Details(int? Id)
        {
            return View(db.News.Where(x => x.ID == Id).FirstOrDefault());
        }
    }
}