﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Helpers;
using Core.Viewmodels.Admin;
using Mallamed.Entity;
namespace Mallamed.Controllers
{
    public class LandmarkController : Controller
    {
        private readonly MallamDbFEntities db = new MallamDbFEntities();

        public ActionResult Index()
        {
            return View(db.LandmarkTypes.ToList());
        }



        public ActionResult Details(int? Id)
        {
            return View(db.Landmarks.Where(x => x.Id == Id).FirstOrDefault());
        }
        public ActionResult GetAllByLandmarkId(int? LandmarkTypeId,int? RegionId)
        {
            //var model = db.Landmarks.Where(x => x.LandmarkId == LandmarkId).ToList();
            ViewBag.LandmarkTypeId = LandmarkTypeId;
            ViewBag.RegionId = RegionId;
            return View();

        }

        public ActionResult RegionList(int? LandmarkTypeId)
        {
            ViewBag.LandmarkTypeId = LandmarkTypeId;

            var RegionList = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            RegionList.AddRange("Sp_RegionDrop".ExecuParamsSqlOrStored(false).AsList<RegionDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.ID.ToString(),
                Selected = s.ID == 0 ? true : false
            }).ToList());
            ViewBag.RegionId = RegionList;
            var lst3 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst3.AddRange("Sp_CityDrop".ExecuParamsSqlOrStored(false).AsList<CityDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.Cities = lst3;
            return PartialView("RegionList");

        }
        public ActionResult LandmarkList(int LandmarkTypeId, int? RegionId)
        {
            if (RegionId == null )
            {
                return View(db.Landmarks.Where(x => x.LandmarkId == LandmarkTypeId).ToList());
            }
            else
            {
                return View(db.Landmarks.Where(x => x.LandmarkId == LandmarkTypeId && x.RegionId==RegionId).ToList());

            }
        }

    }
}