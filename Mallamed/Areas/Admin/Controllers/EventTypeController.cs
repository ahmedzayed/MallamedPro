﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class EventTypeController : Controller
    {
        // GET: Admin/EventType
        public ActionResult Index()
        {
            var model = "SP_GetAllEventType".ExecuParamsSqlOrStored(false).AsList<EventTypeVM>();
            return View(model);
        }


        [HttpGet]
        public ActionResult Create()
        {
           EventTypeVM obj = new EventTypeVM();
            return PartialView("~/Areas/Admin/Views/EventType/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = "SP_SelectEventTypeById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<EventTypeVM>();
            return PartialView("~/Areas/Admin/Views/EventType/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(LandMArkTypeVm model)
        {



            var data = "SP_AddEventType".ExecuParamsSqlOrStored(false, "name".KVP(model.Name), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("Index");


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            try
            {
                var data = "SP_DeleteEventType".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
                if (data != 0)
                {
                    TempData["msg"] = "<script>alert('تم الحذف ');</script>";

                    return RedirectToAction("Index");


                }
                else{
                    TempData["msg"] = "<script>alert('لم يتم الحذف ');</script>";

                    return RedirectToAction("Index");
                }

            }
            catch
            {
                TempData["msg"] = "<script>alert(' هذا العنصر مرتبط بجول اخر ولا يمكن الحذف ');</script>";

                return RedirectToAction("Index");
            }
        }
    }
}