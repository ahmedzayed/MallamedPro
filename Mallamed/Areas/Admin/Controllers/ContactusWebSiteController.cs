﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class ContactusWebSiteController : Controller
    {
        // GET: Admin/ContactusWebSite
        public ActionResult Index()
        {
            var model = "SP_GetAllContact".ExecuParamsSqlOrStored(false).AsList<ContactWebSiteVM>();
            return View(model);
        }
        public ActionResult Delete(int Id)
        {
            var data = "SP_DeleteContact".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("Index");


            }
            return Json(new { message = "Erro" });


        }
    }
}