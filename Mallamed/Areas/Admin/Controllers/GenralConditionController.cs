﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class GenralConditionController : Controller
    {
        // GET: Admin/GenralCondition
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GeneralConditionList()
        {
            var model = "SP_GetAllcondition".ExecuParamsSqlOrStored(false).AsList<Genralconditionvm>();
            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            Genralconditionvm obj = new Genralconditionvm();
            return PartialView("~/Areas/Admin/Views/GenralCondition/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = "SP_SelectGenralCondtionById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<Genralconditionvm>();
            return PartialView("~/Areas/Admin/Views/GenralCondition/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(Genralconditionvm model)
        {

            var data = "SP_AddCondition".ExecuParamsSqlOrStored(false, "Condition".KVP(model.Condition), "Id".KVP(model.ID)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("GeneralConditionList");


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }
        //public ActionResult EditGeneralCondition(Genralconditionvm model)
        //{


        //    var data = "SP_AddCondition".ExecuParamsSqlOrStored(false, "Condition".KVP(model.Condition), "Id".KVP(model.ID)).AsNonQuery();
        //    if (data != 0)
        //    {
        //        return RedirectToAction("GeneralConditionList");


        //    }


        //    return Json(new { message = "Erro" });

        //}
        public ActionResult DeleteGeneralCondition(int Id)
        {
            try
            {
                var data = "SP_DeleteCondition".ExecuParamsSqlOrStored(false, "id".KVP(Id)).AsNonQuery();
                if (data != 0)
                {
                    TempData["msg"] = "<script>alert('تم الحذف ');</script>";

                    return RedirectToAction("GeneralConditionList");


                }
                else
                {
                    TempData["msg"] = "<script>alert('لم يتم الحذف ');</script>";

                    return RedirectToAction("GeneralConditionList");

                }
            }
            catch
            {
                TempData["msg"] = "<script>alert(' هذا العنصر مرتبط بجول اخر ولا يمكن الحذف ');</script>";
                return RedirectToAction("GeneralConditionList");

            }
        }
    }
}