﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class LandMarkTypeController : Controller
    {
        // GET: Admin/LandMarkType
        public ActionResult Index()
        {
            var model = "SP_GetAllLandMArkType".ExecuParamsSqlOrStored(false).AsList<LandMArkTypeVm>();
            return View(model);
        }


        [HttpGet]
        public ActionResult Create()
        {
            LandMArkTypeVm obj = new LandMArkTypeVm();
            return PartialView("~/Areas/Admin/Views/LandMarkType/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = "SP_SelectLandTypeById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<LandMArkTypeVm>();
            return PartialView("~/Areas/Admin/Views/LandMarkType/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(LandMArkTypeVm model)
        {

           

            var data = "SP_AdLandType".ExecuParamsSqlOrStored(false, "name".KVP(model.Name),  "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("Index");


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            try
            {
                var data = "SP_DeleteLandType".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
                if (data != 0)
                {
                    TempData["msg"] = "<script>alert('تم الحذف ');</script>";

                    return RedirectToAction("Index");


                }
                else
                {
                    TempData["msg"] = "<script>alert('لم يتم الحذف ');</script>";

                    return RedirectToAction("Index");
                }

            }
            catch
            {
                TempData["msg"] = "<script>alert(' هذا العنصر مرتبط بجول اخر ولا يمكن الحذف ');</script>";

                return RedirectToAction("Index");
            }

        }
    }
}