﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class EventSponsorController : Controller
    {
        // GET: Admin/EventSponsor
        public ActionResult Index()
        {
            var model = "SP_GetAllEventSponsor".ExecuParamsSqlOrStored(false).AsList<EventSponserVM>();
            return View(model);
        }

        [HttpGet]
        public ActionResult Create(int RegionId = 0, int EventId = 0)
        {
            EventSponsorFM obj = new EventSponsorFM();

            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("Sp_EventDrop".ExecuParamsSqlOrStored(false).AsList<EventDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == EventId ? true : false
            }).ToList());
            ViewBag.Events = lst2;
            return PartialView("~/Areas/Admin/Views/EventSponsor/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id, int EventId = 0)
        {
            EventSponsorFM obj = new EventSponsorFM();

            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("Sp_EventDrop".ExecuParamsSqlOrStored(false).AsList<EventDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == EventId ? true : false
            }).ToList());
            ViewBag.Events = lst2;
            var model = "SP_SelectEventSponserById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<EventSponsorFM>();
            return PartialView("~/Areas/Admin/Views/EventSponsor/Create.cshtml", model.FirstOrDefault());
        }

        [HttpPost]

        public ActionResult Create(EventSponsorFM model,int? EvSpId)
        {
            if(EvSpId != null)
            {
                model.EventId = (int)EvSpId;
            }
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Uploads/Images/EventSponsor/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    model.PhotoPath = "Images/EventSponsor/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }
            }
            var data = "SP_AddEventSponsor".ExecuParamsSqlOrStored(false, "Name".KVP(model.Name), "PhotoPath".KVP(model.PhotoPath), "LandmarkId".KVP(model.EventId), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("Index");


            }
            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            var data = "SP_DeleteEventSponser".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if(data != 0)
            {
                TempData["msg"] = "<script>alert('تم الحذف ');</script>";

                return RedirectToAction("Index");


            }
            else
            {
                TempData["msg"] = "<script>alert('لم يتم الحذف ');</script>";

                return RedirectToAction("Index");
            }


        }
    }

}
