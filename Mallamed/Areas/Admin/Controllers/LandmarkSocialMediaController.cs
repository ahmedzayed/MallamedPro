﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class LandmarkSocialMediaController : Controller
    {
        // GET: Admin/LandmarkSocialMedia
        public ActionResult Index()
        {
            var model = "SP_GetAllLandMarkSocilamedia".ExecuParamsSqlOrStored(false).AsList<LandmarkscoilmediaVM>();
            return View(model);
        }

        [HttpGet]
        public ActionResult Create(int RegionId = 0, int LandMarkId = 0)
        {
            LandmarkscoilmediaFM obj = new LandmarkscoilmediaFM();

            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("Sp_LandmarksDrop".ExecuParamsSqlOrStored(false).AsList<LandMArkDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == LandMarkId ? true : false
            }).ToList());
            ViewBag.ListLandMark = lst2;
            return PartialView("~/Areas/Admin/Views/LandmarkSocialMedia/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id, int LandMarkId = 0)
        {
            LandmarkscoilmediaFM obj = new LandmarkscoilmediaFM();

            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("Sp_LandmarksDrop".ExecuParamsSqlOrStored(false).AsList<LandMArkDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == LandMarkId ? true : false
            }).ToList());
            ViewBag.ListLandMark = lst2;
            var model = "SP_SelectLandMarkSocialById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<LandmarkscoilmediaFM>();
            return PartialView("~/Areas/Admin/Views/LandmarkSocialMedia/Create.cshtml", model.FirstOrDefault());
        }

        [HttpPost]

        public ActionResult Create(LandmarkscoilmediaFM model,int? IdSo)
        {

            model.Id = (int)IdSo;
            var data = "SP_AdLandmarkSocialmedia".ExecuParamsSqlOrStored(false, "Link".KVP(model.Link),"type".KVP(model.type), "IsActive".KVP(model.IsActive), "LandmarkId".KVP(model.LandmarkId), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("Details", "LandMarks", new { Id = model.LandmarkId });


            }
            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public int Delete(int Id)
        {
            var data = "SP_DeleteLandmarkSocialmedia".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return 1;


            }
            return 0;


        }
    }
}