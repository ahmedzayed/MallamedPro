﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class AboutCardsWebSiteController : Controller
    {
        // GET: Admin/CardsWebSite
        public ActionResult Index()
        {
            return View();
        }



        public ActionResult AboutCardsWebSiteList()
        {
            var model = "SP_GetAllCardAboutCard".ExecuParamsSqlOrStored(false).AsList<CardWebsiteVM>();
            var model1 = model.Where(a => a.Main == true).ToList();
            return View(model1);

        }
        [HttpGet]
        public ActionResult Create()
        {
            CardWebsiteVM obj = new CardWebsiteVM();
            return PartialView("~/Areas/Admin/Views/AboutCardsWebSite/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = "SP_SelectCardWebsiteByIds".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<CardWebsiteVM>();
            return PartialView("~/Areas/Admin/Views/AboutCardsWebSite/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(CardWebsiteVM model)
        {

            var data = "SP_AddCardAbout".ExecuParamsSqlOrStored(false, "name".KVP(model.CardText), "Id".KVP(model.ID)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("AboutCardsWebSiteList");


            }

            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            var data = "SP_DeleteCardwebsite".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("AboutCardsWebSiteList");


            }
            return Json(new { message = "Erro" });


        }
    }
}