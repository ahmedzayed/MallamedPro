﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class ImageWebSiteController : Controller
    {
        // GET: Admin/ImageWebSite
        public ActionResult Index()
        {
            var model = "SP_GetAllImagewebsite".ExecuParamsSqlOrStored(false).AsList<ImageWebSiteVM>();
            return View(model);
        }


        [HttpGet]
        public ActionResult Create()
        {
            ImageWebSiteVM obj = new ImageWebSiteVM();
            return PartialView("~/Areas/Admin/Views/ImageWebSite/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = "SP_SelectImagewebById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<ImageWebSiteVM>();
            return PartialView("~/Areas/Admin/Views/ImageWebSite/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(ImageWebSiteVM model)
        {

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Uploads/Images/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    model.PhotoPath = "Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }
            }


            var data = "SP_AddImageWebSite".ExecuParamsSqlOrStored(false, "Title".KVP(model.Title), "Photo".KVP(model.PhotoPath), "Isactive".KVP(model.Isactive), "Id".KVP(model.ID)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("Index");


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            try
            {
                var data = "SP_DeleteImageWebSite".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
                if (data != 0)
                {
                    TempData["msg"] = "<script>alert('تم الحذف ');</script>";

                    return RedirectToAction("Index");


                }
                else
                {
                    TempData["msg"] = "<script>alert('لم يتم الحذف ');</script>";

                    return RedirectToAction("GeneralConditionList");
                }
            }
            catch
            {
                TempData["msg"] = "<script>alert(' هذا العنصر مرتبط بجول اخر ولا يمكن الحذف ');</script>";
                return RedirectToAction("GeneralConditionList");
            }
        }
    }
}