﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class RegionController : Controller
    {
        // GET: Admin/Region
        public ActionResult Index()
        {
            return View();
        }

      

        public ActionResult RegionList()
        {
            var model = "SP_GetAllRegion".ExecuParamsSqlOrStored(false).AsList<RegionVm>();
            return View(model);

        }
        [HttpGet]
        public ActionResult Create( int CityId=0)
        {
            RegionFM obj = new RegionFM();
            var lst = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst.AddRange("Sp_CityDrop".ExecuParamsSqlOrStored(false).AsList<CityVm>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == CityId ? true : false
            }).ToList());
            ViewBag.ListCities = lst;
            return PartialView("~/Areas/Admin/Views/Region/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id, int CityId=0)
        {

            var model = "SP_SelectRegionByIds".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<RegionFM>();
            var lst = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst.AddRange("Sp_CityDrop".ExecuParamsSqlOrStored(false).AsList<CityVm>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == CityId ? true : false
            }).ToList());
            ViewBag.ListCities = lst;
            return PartialView("~/Areas/Admin/Views/Region/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]
       
            public ActionResult Create(RegionFM model)
        {

            var data = "SP_AddRegions".ExecuParamsSqlOrStored(false, "name".KVP(model.Name),"CityId".KVP(model.CityId), "Id".KVP(model.ID)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("RegionList");


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteRegion(int Id)
        {
            try
            {
                var data = "SP_DeleteRegion".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
                if (data != 0)
                {
                    TempData["msg"] = "<script>alert('تم الحذف ');</script>";

                    return RedirectToAction("RegionList");


                }
                else
                {
                    TempData["msg"] = "<script>alert('لم يتم الحذف ');</script>";

                    return RedirectToAction("RegionList");
                }
            }
            catch
            {
                TempData["msg"] = "<script>alert(' هذا العنصر مرتبط بجول اخر ولا يمكن الحذف ');</script>";
                return RedirectToAction("GeneralConditionList");
            }

        }
    }
}