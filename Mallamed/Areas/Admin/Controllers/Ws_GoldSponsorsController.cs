﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    public class Ws_GoldSponsorsController : Controller
    {
        // GET: Admin/Ws_GoldSponsors
        public ActionResult Index()
        {
            var model = "SP_GetAllWs_GoldSponsors".ExecuParamsSqlOrStored(false).AsList<Ws_GoldSponsorsVM>();
            return View(model);
        }


        [HttpGet]
        public ActionResult Create()
        {
            Ws_GoldSponsorsVM obj = new Ws_GoldSponsorsVM();
            return PartialView("~/Areas/Admin/Views/Ws_GoldSponsors/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = "SP_SelectWs_GoldSponsorsById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<Ws_GoldSponsorsVM>();
            return PartialView("~/Areas/Admin/Views/Ws_GoldSponsors/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(Ws_GoldSponsorsVM model)
        {

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Uploads/Images/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    model.PhotoPath = "Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }
            }


            var data = "SP_AddWs_GoldSponsors".ExecuParamsSqlOrStored(false, "Title".KVP(model.Name), "PhotoPath".KVP(model.PhotoPath), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("Index");


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            try
            {
                var data = "SP_DeleteWs_GoldSponsors".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
                if (data != 0)
                {
                    TempData["msg"] = "<script>alert('تم الحذف ');</script>";

                    return RedirectToAction("Index");


                }
                else
                {
                    TempData["msg"] = "<script>alert('لم يتم الحذف ');</script>";

                    return RedirectToAction("Index");
                }
            }
            catch
            {
                TempData["msg"] = "<script>alert(' هذا العنصر مرتبط بجول اخر ولا يمكن الحذف ');</script>";

                return RedirectToAction("Index");

            }
        }
    }
}