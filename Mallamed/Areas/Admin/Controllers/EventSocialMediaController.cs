﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class EventSocialMediaController : Controller
    {
        // GET: Admin/EventSocialMedia
        public ActionResult Index()
        {
            var model = "SP_GetAllEventSocilamedia".ExecuParamsSqlOrStored(false).AsList<EventSocalVM>();
            return View(model);
        }

        [HttpGet]
        public ActionResult Create(int RegionId = 0, int EventId = 0)
        {
            EventSocialFM obj = new EventSocialFM();

            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("Sp_EventDrop".ExecuParamsSqlOrStored(false).AsList<EventDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == EventId ? true : false
            }).ToList());
            ViewBag.Events = lst2;
            return PartialView("~/Areas/Admin/Views/EventSocialMedia/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id, int EventId = 0)
        {
            EventSocialFM obj = new EventSocialFM();

            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("Sp_EventDrop".ExecuParamsSqlOrStored(false).AsList<EventDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == EventId ? true : false
            }).ToList());
            ViewBag.Events = lst2;
            var model = "SP_SelectEventSocialById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<EventSocialFM>();
            return PartialView("~/Areas/Admin/Views/EventSocialMedia/Create.cshtml", model.FirstOrDefault());
        }

        [HttpPost]

        public ActionResult Create(EventSocialFM model, int? EvSoId)
        {

            if (EvSoId != null)
            {
                model.Id = (int)EvSoId;
            }
            var data = "SP_AdEventSocialmedia".ExecuParamsSqlOrStored(false, "Link".KVP(model.Link), "type".KVP(model.type), "IsActive".KVP(model.IsActive), "LandmarkId".KVP(model.EventId), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("Details", "AdminEvents", new { id = model.EventId });


            }
            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            var data = "SP_DeleteEventSocialmedia".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                TempData["msg"] = "<script>alert('تم الحذف ');</script>";

                return RedirectToAction("Index");


            }
            else
            {
                TempData["msg"] = "<script>alert('لم يتم الحذف ');</script>";

                return RedirectToAction("Index");
            }


        }
    }

}
  