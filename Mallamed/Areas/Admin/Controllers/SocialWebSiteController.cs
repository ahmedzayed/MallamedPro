﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    public class SocialWebSiteController : Controller
    {
        // GET: Admin/SocialWebSite
        public ActionResult Index()
        {
            return View();
        }



        public ActionResult SocialWebSiteList()
        {
            var model = "SP_GetAllSocila".ExecuParamsSqlOrStored(false).AsList<SocialWebSiteVM>();
            return View(model);

        }
        [HttpGet]
        public ActionResult Create()
        {
            SocialWebSiteVM obj = new SocialWebSiteVM();
            return PartialView("~/Areas/Admin/Views/SocialWebSite/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = "SP_SelecSocialWebsiteByIds".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<SocialWebSiteVM>();
            return PartialView("~/Areas/Admin/Views/SocialWebSite/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(SocialWebSiteVM model)
        {

            var data = "SP_AddSocialwebsite".ExecuParamsSqlOrStored(false, "Link".KVP(model.Link),"type".KVP(model.type), "Isactive".KVP(model.Isactive), "Id".KVP(model.ID)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("SocialWebSiteList");


            }

            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            try
            {
                var data = "SP_DeleteSocialwebsite".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
                if (data != 0)
                {
                    TempData["msg"] = "<script>alert('تم الحذف ');</script>";

                    return RedirectToAction("SocialWebSiteList");


                }
                else
                {
                    TempData["msg"] = "<script>alert('لم يتم الحذف ');</script>";

                    return RedirectToAction("SocialWebSiteList");
                }
            }
            catch
            {
                TempData["msg"] = "<script>alert(' هذا العنصر مرتبط بجول اخر ولا يمكن الحذف ');</script>";
                return RedirectToAction("GeneralConditionList");
            }
        }
    }
}