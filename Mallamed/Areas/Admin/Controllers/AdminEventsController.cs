﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Mallamed.Entity;
using System.Web.Mvc;
using System.Data.Entity.Migrations;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    /////test
    public class AdminEventsController : Controller
    {
        // GET: Admin/Events
        MallamDbFEntities db = new MallamDbFEntities();
        public ActionResult Index()
        {
            var model = "SP_GetAllEvents".ExecuParamsSqlOrStored(false).AsList<EventVM>();
            return View(model);
        }





        [HttpGet]
        public ActionResult Create(int RegionId = 0, int EventTypeId = 0)
        {
            EventFM obj = new EventFM();
            var RegionList = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            RegionList.AddRange("Sp_RegionDrop".ExecuParamsSqlOrStored(false).AsList<RegionDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.ID.ToString(),
                Selected = s.ID == RegionId ? true : false
            }).ToList());
            ViewBag.RegionId = RegionList;
            var lst3 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst3.AddRange("Sp_CityDrop".ExecuParamsSqlOrStored(false).AsList<CityDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.Cities = lst3;
            var lst = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst.AddRange("Sp_RegionDrop".ExecuParamsSqlOrStored(false).AsList<RegionDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.ID.ToString(),
                Selected = s.ID == RegionId ? true : false
            }).ToList());
            ViewBag.ListRegion = lst;
            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("Sp_EventTypeDrop".ExecuParamsSqlOrStored(false).AsList<EventTypeVM>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == EventTypeId ? true : false
            }).ToList());
            ViewBag.EventType = lst2;
            return PartialView("~/Areas/Admin/Views/AdminEvents/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id, int RegionId = 0, int EventTypeId = 0)
        {
            EventFM obj = new EventFM();
            var RegionList = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            RegionList.AddRange("Sp_RegionDrop".ExecuParamsSqlOrStored(false).AsList<RegionDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.ID.ToString(),
                Selected = s.ID == RegionId ? true : false
            }).ToList());
            ViewBag.RegionId = RegionList;
            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };

            lst2.AddRange("Sp_EventTypeDrop".ExecuParamsSqlOrStored(false).AsList<EventTypeVM>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == EventTypeId ? true : false
            }).ToList());
            ViewBag.EventType = lst2;
            var lst3 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst3.AddRange("Sp_CityDrop".ExecuParamsSqlOrStored(false).AsList<CityDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.Cities = lst3;
            var model = "SP_SelectEventById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<EventFM>();
            return PartialView("~/Areas/Admin/Views/AdminEvents/Create.cshtml", model.FirstOrDefault());
        }

        [HttpPost]

        public ActionResult Create(EventFM model)
        {

            try
            {

                if (Request.Files.Count > 0)
                {
                    var file = Request.Files[0];

                    if (file != null && file.ContentLength > 0)
                    {
                        var fileNameWithExtension = Path.GetFileName(file.FileName);
                        var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                        string RootPath = Server.MapPath("~/Uploads/Images/Event/");
                        if (!Directory.Exists(RootPath))
                        {
                            Directory.CreateDirectory(RootPath);
                        }
                        var unique = new Guid();
                        var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                        file.SaveAs(path);
                        model.PhotoPath = "Images/Event/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                    }



                }

                var data = "SP_AdEvent".ExecuParamsSqlOrStored(false, "Name".KVP(model.Name), "Details".KVP(model.Details), "StartDate".KVP(model.StartDate), "EndDate".KVP(model.EndDate), "Lat".KVP(model.Lat), "RegionId".KVP(model.RegionId), "EventTypeId".KVP(model.EventTypeId),
                    "Lang".KVP(model.Lang), "PhotoPath".KVP(model.PhotoPath), "Address".KVP(model.Address),"CityId".KVP(model.CityId), "Reference".KVP(model.Reference) ,"Id".KVP(model.Id)).AsNonQuery();
                if (data != 0)
                {
                    TempData["msg"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                    if (model.Id != 0)
                    {
                        return RedirectToAction("Details", new { id = model.Id });
                    }
                    else
                    {
                        return RedirectToAction("Index");

                    }

                }
            }
            catch
            {
                TempData["msg"] = "<script>alert(' من فصلك ادخل تاريخ البدايه والنهايه  ');</script>";

                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int Id)
        {
            try
            {
                var data = "SP_DeleteEvent".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
                if (data != 0)
                {
                    TempData["msg"] = "<script>alert(' تم الحذف');</script>";

                    return RedirectToAction("Index");


                }
                return Json(new { message = "Erro" });

            }
            catch
            {
                TempData["msg"] = "<script>alert(' هذا العنصر مرتبط بجول اخر ولا يمكن الحذف ');</script>";

                return RedirectToAction("Index");
            }
        }


        public ActionResult Details(int? id)
        {
            if (id == null)
                return RedirectToAction("Index");
            var Ev = db.Events.FirstOrDefault(c => c.Id == id);
            if (Ev == null)
                return RedirectToAction("Index");
            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);
            ViewBag.Regions = new SelectList(db.Regions.ToList(), "ID", "Name");
            ViewBag.Eventtype = new SelectList(db.EventTypes.ToList(), "Id", "Name");
            ViewBag.Cards = new SelectList(db.Cards.ToList(), "ClientID", "Name");


            return View(Ev);
        }


        public ActionResult ChangeLocation(int? id, string lat, string lang)
        {
            var lan = db.Events.Find(id);
            lan.Lat = lat;
            lan.Lang = lang;
            db.Events.AddOrUpdate(lan);
            db.SaveChanges();
            return RedirectToAction("Details", "AdminEvents", new { id = id });
        }
    }
}