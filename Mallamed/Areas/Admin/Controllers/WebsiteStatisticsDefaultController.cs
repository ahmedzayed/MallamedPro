﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    public class WebsiteStatisticsDefaultController : Controller
    {
        // GET: Admin/WebsiteStatisticsDefault
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult WebsiteStatisticsDefaultList()
        {
            var model = "SP_WebsiteStatistics".ExecuParamsSqlOrStored(false).AsList<WebsiteStatisticsVm>();
            return View(model);
        }


        [HttpGet]
        public ActionResult Create()
        {
            WebsiteStatisticsVm obj = new WebsiteStatisticsVm();
            return PartialView("~/Areas/Admin/Views/WebsiteStatisticsDefault/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = "SP_SelectWebsiteStatisticsdefultById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<WebsiteStatisticsVm>();
            return PartialView("~/Areas/Admin/Views/WebsiteStatisticsDefault/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(WebsiteStatisticsVm model)
        {



            var data = "SP_AddWebsiteStatisticsDefault".ExecuParamsSqlOrStored(false, "Title".KVP(model.Title), "Count".KVP(model.Count), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("WebsiteStatisticsDefaultList");


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

       
        public ActionResult DeleteWebsiteStatisticsDefault(int Id)
        {
            var data = "SP_DeleteWebsiteStatisticsDefault".ExecuParamsSqlOrStored(false, "id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("WebsiteStatisticsDefaultList");


            }
            return Json(new { message = "Erro" });


        }


    }
}