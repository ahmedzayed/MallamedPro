﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class LandmarkWorkTimeController : Controller
    {
        // GET: Admin/landmarkWorkTime
        public ActionResult Index()
        {
            var model = "SP_GetAllLandMarkTime".ExecuParamsSqlOrStored(false).AsList<LandmarkTimeVM>();
            return View(model);
        }

        [HttpGet]
        public ActionResult Create( int LandMarkId = 0)
        {
            LandmarkTimeFM obj = new LandmarkTimeFM();

            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("Sp_LandmarksDrop".ExecuParamsSqlOrStored(false).AsList<LandMArkDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == LandMarkId ? true : false
            }).ToList());
            ViewBag.ListLandMark = lst2;
            return PartialView("~/Areas/Admin/Views/LandmarkWorkTime/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id, int LandMarkId = 0)
        {
            LandmarkPhonesFM obj = new LandmarkPhonesFM();

            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("Sp_LandmarksDrop".ExecuParamsSqlOrStored(false).AsList<LandMArkDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == LandMarkId ? true : false
            }).ToList());
            ViewBag.ListLandMark = lst2;
            var model = "SP_SelectLandMarkTimeById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<LandmarkTimeFM>();
            return PartialView("~/Areas/Admin/Views/LandmarkWorkTime/Create.cshtml", model.FirstOrDefault());
        }

        [HttpPost]

        public ActionResult Create(LandmarkTimeFM model , int? IdTime )
        {

            model.Id = (int)IdTime;
            var data = "SP_AdLandmarkTime".ExecuParamsSqlOrStored(false, "ValidTimeTo".KVP(model.ValidTimeTo),
                "ValidTimeFrom".KVP(model.ValidTimeFrom), "ValidDayFrom".KVP(model.ValidDayFrom), "ValidDayTo".KVP(model.ValidDayTo), "LandmarkId".KVP(model.LandmarkId), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("Details", "LandMarks",new { Id=model.LandmarkId});


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public int Delete(int Id, int LandmarkId)
        {
            
            var data = "SP_DeleteLandmarkTime".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return 1;
            }
            return 0;

        }
    }
}