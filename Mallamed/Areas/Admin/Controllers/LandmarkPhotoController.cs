﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class LandmarkPhotoController : Controller
    {
        // GET: Admin/LandmarkPhoto
        public ActionResult Index()
        {
            var model = "SP_GetAllLandPhoto".ExecuParamsSqlOrStored(false).AsList<LandMArkPhotoVm>();
            return View(model);
        }

        [HttpGet]
        public ActionResult Create(int CardSID = 0, int LandMarkId = 0)
        {
            LandMArkPhotoFm obj = new LandMArkPhotoFm();
            var lst = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst.AddRange("Sp_CardDrop".ExecuParamsSqlOrStored(false).AsList<CardsVM>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.ClientID.ToString(),
                Selected = s.ClientID == CardSID ? true : false
            }).ToList());
            ViewBag.ListCards = lst;
            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("Sp_LandmarksDrop".ExecuParamsSqlOrStored(false).AsList<LandMArkDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == LandMarkId ? true : false
            }).ToList());
            ViewBag.ListLandMark = lst2;
            return PartialView("~/Areas/Admin/Views/LandMarkPhoto/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id, int CardSID = 0, int LandMarkId = 0)
        {
            LandMArkPhotoFm obj = new LandMArkPhotoFm();
            var lst = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst.AddRange("Sp_CardDrop".ExecuParamsSqlOrStored(false).AsList<CardsVM>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.ClientID.ToString(),
                Selected = s.ClientID == CardSID ? true : false
            }).ToList());
            ViewBag.ListCards = lst;
            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("Sp_LandmarksDrop".ExecuParamsSqlOrStored(false).AsList<LandMArkDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == LandMarkId ? true : false
            }).ToList());
            ViewBag.ListLandMark = lst2;
            var model = "SP_SelectLandmarkPhotoByID".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<LandMArkPhotoFm>();
            return PartialView("~/Areas/Admin/Views/LandMarkPhoto/Create.cshtml", model.FirstOrDefault());
        }

        [HttpPost]

        public ActionResult Create(LandMArkPhotoFm model,int? IdPhoto)
        {
            if (IdPhoto != null)
            {

                model.Id = (int)IdPhoto;
            }
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Uploads/Images/LandMark/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    model.PhotoPath = "Images/LandMark/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }



            }

            var data = "SP_AdLandmarkPhoto".ExecuParamsSqlOrStored(false, "PhotoPath".KVP(model.PhotoPath), "IsSuggestion".KVP(model.IsSuggestion), "LandmarkId".KVP(model.LandmarkId), "CardId".KVP(model.CardId), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("Details", "LandMarks", new { Id = model.LandmarkId });


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public int Delete(int Id)
        {
            var data = "SP_DeleteLandMarkPhoto".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return 1;


            }
            return 2;


        }

    }
}