﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mallamed.Entity;
using System.Data.Entity.Migrations;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class LandMarksController : Controller
       
    {
        MallamDbFEntities db = new MallamDbFEntities();
        // GET: Admin/LandMarks
        public ActionResult Index()
        {
            var model = "SP_GetAllLandMark".ExecuParamsSqlOrStored(false).AsList<LandMarksVm>();
            return View(model);
        }

        [HttpGet]
        public ActionResult Create(int RegionId = 0, int LandMarkId = 0, int CityId = 0)
        {
            var RegionList = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            RegionList.AddRange("Sp_RegionDrop".ExecuParamsSqlOrStored(false).AsList<RegionDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.ID.ToString(),
                Selected = s.ID == RegionId ? true : false
            }).ToList());
            ViewBag.RegionId = RegionList;
            LandMarkFm obj = new LandMarkFm();
            var lst = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst.AddRange("Sp_RegionDrop".ExecuParamsSqlOrStored(false).AsList<RegionDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.ID.ToString(),
                Selected = s.ID == RegionId ? true : false
            }).ToList());
            ViewBag.ListRegion = lst;
            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("Sp_LandmarkDrop".ExecuParamsSqlOrStored(false).AsList<LandMArkTypeVm>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == LandMarkId ? true : false
            }).ToList());
            ViewBag.ListLandMark = lst2;

            var lst3 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst3.AddRange("Sp_CityDrop".ExecuParamsSqlOrStored(false).AsList<CityDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == CityId ? true : false
            }).ToList());
            ViewBag.Cities = lst3;
            return View("~/Areas/Admin/Views/LandMarks/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id,int RegionId=0,int LandMarkId=0)
        {
            LandMarkFm obj = new LandMarkFm();
            var RegionList = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            RegionList.AddRange("Sp_RegionDrop".ExecuParamsSqlOrStored(false).AsList<RegionDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.ID.ToString(),
                Selected = s.ID == RegionId ? true : false
            }).ToList());
            ViewBag.RegionId = RegionList;
            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("Sp_LandmarkDrop".ExecuParamsSqlOrStored(false).AsList<LandMArkTypeVm>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == LandMarkId ? true : false
            }).ToList());
            ViewBag.ListLandMark = lst2;
            var lst3 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst3.AddRange("Sp_CityDrop".ExecuParamsSqlOrStored(false).AsList<CityDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.Cities = lst3;
            var model = "SP_SelectLandMarkById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<LandMarkFm>();
            return PartialView("~/Areas/Admin/Views/LandMarks/Create.cshtml", model.FirstOrDefault());
        }

        [HttpPost]

        public ActionResult Create(LandMarkFm model)
        {


            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Uploads/Images/LandMark/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    model.PhotoPath = "Images/LandMark/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }

                var file1 = Request.Files[1];

                if (file1 != null && file1.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file1.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file1.FileName);
                    string RootPath = Server.MapPath("~/Uploads/Images/LandMark/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file1.FileName));
                    file1.SaveAs(path);
                    model.VideoPath = "Images/LandMark/" + fileNameOnly + unique + Path.GetExtension(file1.FileName);

                }

            }

            var data = "SP_AdLandmark".ExecuParamsSqlOrStored(false, "name".KVP(model.Name), "Details".KVP(model.Details),"Email".KVP(model.Email),"Longx".KVP(model.LongX),"LatX".KVP(model.LatX),"RegionId".KVP(model.RegionId),"LandmarkId".KVP(model.LandmarkID),
                "VideoPath".KVP(model.VideoPath),"PhotoPath".KVP(model.PhotoPath),"AudioPath".KVP(model.AudioPath),"CopyRight".KVP(model.CopyRight),"Owner".KVP(model.Owner),"CityId".KVP(model.CityId), "Reference".KVP(model.Reference),"Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("Index");


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            try
            {
                var data = "SP_DeleteLandMark".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
                if (data != 0)
                {
                    TempData["msg"] = "<script>alert('تم الحذف ');</script>";

                    return RedirectToAction("Index");


                }
                else
                {
                    TempData["msg"] = "<script>alert('لم يتم الحذف ');</script>";

                    return RedirectToAction("Index");
                }
            }
            catch
            {
                TempData["msg"] = "<script>alert(' هذا العنصر مرتبط بجول اخر ولا يمكن الحذف ');</script>";
                return RedirectToAction("Index");

            }

        }

        public ActionResult Details(int? id ,int CardSID=0)
        {
            if (id == null)
                return RedirectToAction("Index");
            var lst = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst.AddRange("Sp_CardDrop".ExecuParamsSqlOrStored(false).AsList<CardsVM>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.ClientID.ToString(),
                Selected = s.ClientID == CardSID ? true : false
            }).ToList());
            ViewBag.ListCards = lst;
            var don = db.Landmarks.FirstOrDefault(c => c.Id == id);
            if (don == null)
                return RedirectToAction("Index");
            var err = TempData["error"] != null ? TempData["error"].ToString() : "";
            ModelState.AddModelError("", err);
            ViewBag.Cards = new SelectList(db.Cards.ToList(), "ClientID", "Name");

            ViewBag.Regions = new SelectList(db.Regions.ToList(), "ID", "Name");
            ViewBag.LandmarkTyps = new SelectList(db.LandmarkTypes.ToList(), "Id", "Name");
            //ViewBag.CatID = new SelectList(db.Categorys, "ID", "Name", don.CatID);
            //ViewBag.RegionId = new SelectList(db.Regions, "ID", "Name", don.RegionId);
            //ViewBag.cons = db.GeneralConditions.ToList();

            return View(don);
        }


        public ActionResult ChangeLocation(int? id, string lat, string lang)
        {
            var lan = db.Landmarks.Find(id);
            lan.LongX = lat;
            lan.LatX = lang;
            db.Landmarks.AddOrUpdate(lan);
            db.SaveChanges();
            return RedirectToAction("Details", "LandMarks", new { id = id });
        }

        public ActionResult GetRegion(int Id,int RegionId=0)
        {
            var RegionList = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            RegionList.AddRange("Sp_RegionDropBycity".ExecuParamsSqlOrStored(false,"Id".KVP(Id)).AsList<RegionDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.ID.ToString(),
                Selected = s.ID == RegionId ? true : false
            }).ToList());
            ViewBag.RegionId = RegionList;
            return PartialView("GetCity");
        }
      

    }
}