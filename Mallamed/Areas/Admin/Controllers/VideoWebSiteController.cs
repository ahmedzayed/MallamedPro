﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    public class VideoWebSiteController : Controller
    {
        // GET: Admin/VideoWebSite
        public ActionResult Index()
        {
            var model = "SP_GetAllVideowebsite".ExecuParamsSqlOrStored(false).AsList<VideoWebSiteVM>();
            return View(model);
        }


        [HttpGet]
        public ActionResult Create()
        {
            VideoWebSiteVM obj = new VideoWebSiteVM();
            return PartialView("~/Areas/Admin/Views/VideoWebSite/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = "SP_SelectvideoByIds".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<VideoWebSiteVM>();
            return PartialView("~/Areas/Admin/Views/VideoWebSite/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(VideoWebSiteVM model)
        {

            var data = "SP_AddVideoWebsite".ExecuParamsSqlOrStored(false, "Title".KVP(model.Title), "VideoPath".KVP(model.VideoPath), "Isactive".KVP(model.Isactive), "Id".KVP(model.ID)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("Index");


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            try
            {
                var data = "SP_DeleteVideo".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
                if (data != 0)
                {
                    TempData["msg"] = "<script>alert('تم الحذف ');</script>";

                    return RedirectToAction("Index");


                }
                else
                {
                    TempData["msg"] = "<script>alert('لم يتم الحذف ');</script>";
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                TempData["msg"] = "<script>alert(' هذا العنصر مرتبط بجول اخر ولا يمكن الحذف ');</script>";
                return RedirectToAction("GeneralConditionList");
            }
        }
    }
}