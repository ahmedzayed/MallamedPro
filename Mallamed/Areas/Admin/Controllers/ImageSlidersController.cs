﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class ImageSlidersController : Controller
    {
        // GET: Admin/ImageSliders
        public ActionResult Index()
        {
            var model = "SP_GetAllSlider".ExecuParamsSqlOrStored(false).AsList<ImageSlidersVM>();
            return View(model);
        }


        [HttpGet]
        public ActionResult Create()
        {
            ImageSlidersVM obj = new ImageSlidersVM();
            return PartialView("~/Areas/Admin/Views/ImageSliders/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = "SP_SelectSliderById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<ImageSlidersVM>();
            return PartialView("~/Areas/Admin/Views/ImageSliders/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(ImageSlidersVM model)
        {

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Uploads/Images/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    model.PhotoPath = "Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }
            }


            var data = "SP_AddSlider".ExecuParamsSqlOrStored(false, "Title".KVP(model.Title), "Details".KVP(model.Details), "PhotoPath".KVP(model.PhotoPath), "IsActive".KVP(model.IsActive), "Id".KVP(model.ID)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("Index");


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteImageSliders(int Id)
        {
            try
            {
                var data = "SP_DeleteSlider".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
                if (data != 0)
                {
                    TempData["msg"] = "<script>alert('تم الحذف ');</script>";

                    return RedirectToAction("Index");


                }
                else
                {
                    TempData["msg"] = "<script>alert('لم يتم الحذف ');</script>";

                    return RedirectToAction("Index");
                }
            }
            catch
            {
                TempData["msg"] = "<script>alert(' هذا العنصر مرتبط بجول اخر ولا يمكن الحذف ');</script>";

                return RedirectToAction("Index");

            }
        }
    }
}