﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class CategoryController : Controller
    {
        // GET: Admin/Category
        public ActionResult Index()
        {
            var model = "SP_GetAllCategorys".ExecuParamsSqlOrStored(false).AsList <CategorysVm>();
            return View(model);
        }


        [HttpGet]
        public ActionResult Create()
        {
            CategorysVm obj = new CategorysVm();
            return PartialView("~/Areas/Admin/Views/Category/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = "SP_SelectCatById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<CategorysVm>();
            return PartialView("~/Areas/Admin/Views/Category/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(CategorysVm model)
        {

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Uploads/Images/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    model.Photo ="Images/"+ fileNameOnly + unique + Path.GetExtension(file.FileName);

                }
            }
           
           
            var data = "SP_AdCategory".ExecuParamsSqlOrStored(false, "name".KVP(model.Name), "Photo".KVP(model.Photo), "Id".KVP(model.ID)).AsNonQuery();
            if (data != 0)
            {
                TempData["msg"] = "<script>alert(' تم الاضافه بنجاح');</script>";

                return RedirectToAction("Index");


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteCategory(int Id)
        {
            try{
                var data = "SP_DeleteCategory".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
                if (data != 0)
                {
                    TempData["msg"] = "<script>alert(' تم الحذف');</script>";

                    return RedirectToAction("Index");


                }
                return Json(new { message = "Erro" });

            }
            catch
            {
                TempData["msg"] = "<script>alert(' هذا العنصر مرتبط بجول اخر ولا يمكن الحذف ');</script>";
                return RedirectToAction("Index");

            }

        }
    }
}