﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class EventPhotoController : Controller
    {
        public ActionResult Index()
        {
            var model = "SP_GetAllEventhoto".ExecuParamsSqlOrStored(false).AsList<EventPhotoVM>();
            return View(model);
        }

        [HttpGet]
        public ActionResult Create(int CardSID = 0, int EventId = 0)
        {
            EventPhotoFM obj = new EventPhotoFM();
            var lst = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst.AddRange("Sp_CardDrop".ExecuParamsSqlOrStored(false).AsList<CardsVM>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.ClientID.ToString(),
                Selected = s.ClientID == CardSID ? true : false
            }).ToList());
            ViewBag.ListCards = lst;
            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("Sp_EventDrop".ExecuParamsSqlOrStored(false).AsList<EventDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == EventId ? true : false
            }).ToList());
            ViewBag.Events = lst2;
            return PartialView("~/Areas/Admin/Views/EventPhoto/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id, int CardSID = 0, int EventId = 0)
        {
            EventPhotoFM obj = new EventPhotoFM();
            var lst = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst.AddRange("Sp_CardDrop".ExecuParamsSqlOrStored(false).AsList<CardsVM>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.ClientID.ToString(),
                Selected = s.ClientID == CardSID ? true : false
            }).ToList());
            ViewBag.ListCards = lst;
            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("Sp_EventDrop".ExecuParamsSqlOrStored(false).AsList<EventDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == EventId ? true : false
            }).ToList());
            ViewBag.Events = lst2;
            var model = "SP_SelectEventPhotoByID".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<EventPhotoFM>();
            return PartialView("~/Areas/Admin/Views/EventPhoto/Create.cshtml", model.FirstOrDefault());
        }

        [HttpPost]

        public ActionResult Create(EventPhotoFM model, int? IdPhoto)
        {

            if(IdPhoto != null)
            {
                model.Id =(int) IdPhoto;
            }
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Uploads/Images/EventPhoto/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    model.PhotoPath = "Images/EventPhoto/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }



            }

            var data = "SP_AdEventPhoto".ExecuParamsSqlOrStored(false, "PhotoPath".KVP(model.PhotoPath), "IsSuggestion".KVP(model.IsSuggestion), "EventId".KVP(model.EventId), "CardId".KVP(model.CardId), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("Details", "AdminEvents", new { id=model.EventId});


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public int Delete(int Id)
        {
            try
            {
                var data = "SP_DeleteEventPhoto".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
                if (data != 0)
                {

                    return 1;



                }
                else
                    return 0;
            }
            catch
            {
                TempData["msg"] = "<script>alert(' هذا العنصر مرتبط بجول اخر ولا يمكن الحذف ');</script>";
                return 0;
            }

        }

    }
}