﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class WithdrawalsController : Controller
    {
        // GET: Admin/Withdrawals
        public ActionResult Index()
        {
            var model = "SP_GetAllWithedrawals".ExecuParamsSqlOrStored(false).AsList<WithedrawalsVM>();
            return View(model);
        }
        public ActionResult Delete(int Id)
        {
            var data = "SP_DeleteWite".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("Index");


            }
            return Json(new { message = "Erro" });


        }
    }
}