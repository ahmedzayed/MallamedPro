﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class GoalsController : Controller
    {
        public ActionResult Index()
        {
            var model = "SP_GetAllGoals".ExecuParamsSqlOrStored(false).AsList<GoalsVM>();
            var model1 = model.Where(a => a.Main == false);
            return View(model1.ToList());
        }



      
        [HttpGet]
        public ActionResult Create()
        {
            GoalsVM obj = new GoalsVM();
            return PartialView("~/Areas/Admin/Views/Goals/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = "SP_SelectGoalsByIds".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<GoalsVM>();
            return PartialView("~/Areas/Admin/Views/Goals/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(GoalsVM model)
        {

            var data = "SP_AddGoals".ExecuParamsSqlOrStored(false, "Details".KVP(model.Details), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("Index");


            }

            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            try
            {
                var data = "SP_DeleteGoals".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
                if (data != 0)
                {
                    TempData["msg"] = "<script>alert('تم الحذف ');</script>";

                    return RedirectToAction("Index");


                }
                else
                {
                    TempData["msg"] = "<script>alert('لم يتم الحذف ');</script>";

                    return RedirectToAction("Index");
                }
            }
            catch
            {
                TempData["msg"] = "<script>alert(' هذا العنصر مرتبط بجول اخر ولا يمكن الحذف ');</script>";
                return RedirectToAction("Index");

            }


        }
    }
}