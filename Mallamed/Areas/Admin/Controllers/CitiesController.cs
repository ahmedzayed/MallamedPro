﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{

    [Authorize(Roles = "Admin")]

    public class CitiesController : Controller
    {
        // GET: Admin/CardCategory
        public ActionResult Index()
        {
            return View();
        }



        public ActionResult CityList()
        {
            var model = "SP_GetAllCity".ExecuParamsSqlOrStored(false).AsList<CityVm>();
            return View(model);

        }
        [HttpGet]
        public ActionResult Create()
        {
            CityVm obj = new CityVm();
            return PartialView("~/Areas/Admin/Views/Cities/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = "SP_SelectCitiesById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<CityVm>();
            return PartialView("~/Areas/Admin/Views/Cities/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(CityVm model)
        {

            var data = "SP_AddCities".ExecuParamsSqlOrStored(false, "name".KVP(model.Name),  "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                TempData["msg"] = "<script>alert(' تم الاضافه بنجاح');</script>";

                return RedirectToAction("CityList");


            }

            return RedirectToAction("CityList");
        }

        public ActionResult Delete(int Id)
        {
            try
            {
                var data = "SP_DeleteCities".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
                if (data != 0)
                {
                    TempData["msg"] = "<script>alert(' تم الحذف');</script>";

                    return RedirectToAction("CityList");


                }
                return RedirectToAction("CityList");
            }
            catch
            {
                TempData["msg"] = "<script>alert(' هذا العنصر مرتبط بجول اخر ولا يمكن الحذف ');</script>";

                return RedirectToAction("CityList");
            }

        }
    }
}