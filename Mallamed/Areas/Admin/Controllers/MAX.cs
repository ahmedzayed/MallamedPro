﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mallamed.Areas.Admin.Controllers
{
    public static class MAx
    {
        public static int UserID(ApplicationUserManager userManager)
        {
            var users = userManager.Users.ToList();
            if (users.Any())
                return users.Max(c => c.UserID) + 1;
            return 1;
        }
    }
}