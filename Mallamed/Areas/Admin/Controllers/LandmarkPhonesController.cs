﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class LandmarkPhonesController : Controller
    {
        // GET: Admin/landmarkphones
        public ActionResult Index()
        {
            var model = "SP_GetAllLandMarkPhones".ExecuParamsSqlOrStored(false).AsList<LandmarkPhonesVM>();
            return View(model);
        }

        [HttpGet]
        public ActionResult Create(int RegionId = 0, int LandMarkId = 0)
        {
            LandmarkPhonesFM obj = new LandmarkPhonesFM();

            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("Sp_LandmarksDrop".ExecuParamsSqlOrStored(false).AsList<LandMArkDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == LandMarkId ? true : false
            }).ToList());
            ViewBag.ListLandMark = lst2;
            return PartialView("~/Areas/Admin/Views/LandmarkPhones/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id,int LandMarkId = 0)
        {
            LandmarkPhonesFM obj = new LandmarkPhonesFM();

            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("Sp_LandmarksDrop".ExecuParamsSqlOrStored(false).AsList<LandMArkDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == LandMarkId ? true : false
            }).ToList());
            ViewBag.ListLandMark = lst2;
            var model = "SP_SelectLandMarkPhonesById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<LandmarkPhonesFM>();
            return PartialView("~/Areas/Admin/Views/LandmarkPhones/Create.cshtml", model.FirstOrDefault());
        }

        [HttpPost]

        public ActionResult Create(LandmarkPhonesFM model,int? Idph)
        {
            model.Id =(int) Idph;
            var data = "SP_AdLandmarkPhones".ExecuParamsSqlOrStored(false, "PhoneNumber".KVP(model.PhoneNumber), "LandmarkId".KVP(model.LandmarkId),"Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("Details", "LandMarks", new { Id = model.LandmarkId });


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public int Delete(int Id)
        {
            var data = "SP_DeleteLandmarkPhones".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return 1;


            }
            return 2;


        }
    }
}