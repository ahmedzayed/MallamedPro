﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class AcceptImagesController : Controller
    {
        // GET: Admin/AcceptImages
        public ActionResult Index()
        {
            var model = "SP_GetAllAccImage".ExecuParamsSqlOrStored(false).AsList<AcceptImageVM>();
            return View(model);
        }
    }
}