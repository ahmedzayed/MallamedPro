﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class WebsiteContactInfoController : Controller
    {
        // GET: Admin/WebsiteContactInfo
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            WebSiteContactVm obj = new WebSiteContactVm();
            return PartialView("~/Areas/Admin/Views/WebsiteContactInfo/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = "SP_SelectwebsitecontactById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<WebSiteContactVm>();
            return PartialView("~/Areas/Admin/Views/WebsiteContactInfo/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(WebSiteContactVm model)
        {

            var data = "SP_AddWebsiteContactInfo".ExecuParamsSqlOrStored(false, "PhoneNumber".KVP(model.PhoneNumber), "Address".KVP(model.Address), "TelePhone".KVP(model.TelePhone), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("WebsiteContactInfoList");


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult WebsiteContactInfoList()
        {
            var model = "SP_WebsiteContactInfoList".ExecuParamsSqlOrStored(false).AsList<WebSiteContactVm>();
            return View(model);

        }
     
        public ActionResult DeleteWebsiteContactInfo(int Id)
        {
            var data = "SP_DeleteWebsiteContactInfo".ExecuParamsSqlOrStored(false, "id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("WebsiteContactInfoList");


            }
            return Json(new { message = "Erro" });


        }
    }
}