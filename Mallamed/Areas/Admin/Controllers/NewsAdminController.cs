﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class NewsAdminController : Controller
    {
        // GET: Admin/News
        public ActionResult Index()
        {
            var model = "SP_GetAllNews".ExecuParamsSqlOrStored(false).AsList<NewsVM>();
            return View(model);
        }


        [HttpGet]
        public ActionResult Create()
        {
            NewsVM obj = new NewsVM();
            return PartialView("~/Areas/Admin/Views/NewsAdmin/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = "SP_SelectNewsById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<NewsVM>();
            return PartialView("~/Areas/Admin/Views/NewsAdmin/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(NewsVM model)
        {

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Uploads/Images/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    model.PhotoPath = "Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }
            }

            model.Date = DateTime.Now;
            var data = "SP_AdNews".ExecuParamsSqlOrStored(false, "name".KVP(model.Name), "Photo".KVP(model.PhotoPath),"Details".KVP(model.Details),"Date".KVP(model.Date), "Id".KVP(model.ID)).AsNonQuery();
            if (data != 0)
            {
                var result = "-1";
                var webAddr = "https://fcm.googleapis.com/fcm/send";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);

                httpWebRequest.ContentType = "application/json";
                var firebasekey = System.Configuration.ConfigurationManager.AppSettings["FireBaseKey"];
                httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "key=" + firebasekey);
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    FireBaseData data1 = new FireBaseData();
                    data1.ShortDesc = "Notfication";
                    data1.Description = "تم اضافة  خبر جديد";
                    FireBaseNotfication notfication = new FireBaseNotfication();
                    notfication.title = "Notfication";
                    notfication.text = "تم اضافة خبر جديد";

                    FireBaseObject obj = new FireBaseObject();
                    var Tokens = "SP_GetAllToken".ExecuParamsSqlOrStored(false).AsList<TokensVM>();
                    foreach (var item in Tokens)
                    {
                        obj.to = item.Token;
                        obj.data = data1;
                        obj.notification = notfication;

                        obj.sound = "default";
                        string strNJson = JsonConvert.SerializeObject(obj);

                        streamWriter.Write(strNJson);
                        streamWriter.Flush();
                    }

                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                    Console.WriteLine(streamReader.ReadToEnd());
                }
                return RedirectToAction("Index");


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            try
            {
                var data = "SP_DeleteNews".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
                if (data != 0)
                {
                    TempData["msg"] = "<script>alert('تم الحذف ');</script>";

                    return RedirectToAction("Index");


                }
                else {
                    TempData["msg"] = "<script>alert('لم يتم الحذف ');</script>";

                    return Json(new { message = "Erro" });
                }
            }
            catch
            {
                TempData["msg"] = "<script>alert(' هذا العنصر مرتبط بجول اخر ولا يمكن الحذف ');</script>";
                return RedirectToAction("GeneralConditionList");
            }

        }
    }
}