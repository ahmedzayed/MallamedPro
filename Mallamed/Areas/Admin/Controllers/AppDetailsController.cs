﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    public class AppDetailsController : Controller
    {
        // GET: Admin/AppDetails
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AboutAppList()
        {
            var model = "SP_GetAllAppDetails".ExecuParamsSqlOrStored(false).AsList<AboutAppVM>();
            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            AboutAppVM obj = new AboutAppVM();
            return PartialView("~/Areas/Admin/Views/AppDetails/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = "SP_SelectAppDetailsById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<AboutAppVM>();
            return PartialView("~/Areas/Admin/Views/AppDetails/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(AboutAppVM model)
        {

            var data = "SP_AddAppDetails".ExecuParamsSqlOrStored(false, "Details".KVP(model.Details), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("AboutAppList");


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }
        //public ActionResult EditGeneralCondition(Genralconditionvm model)
        //{


        //    var data = "SP_AddCondition".ExecuParamsSqlOrStored(false, "Condition".KVP(model.Condition), "Id".KVP(model.ID)).AsNonQuery();
        //    if (data != 0)
        //    {
        //        return RedirectToAction("GeneralConditionList");


        //    }


        //    return Json(new { message = "Erro" });

        //}
        public ActionResult DeleteAppDetails(int Id)
        {
            try
            {
                var data = "SP_DeleteAppDetails".ExecuParamsSqlOrStored(false, "id".KVP(Id)).AsNonQuery();
                if (data != 0)
                {
                    TempData["msg"] = "<script>alert('تم الحذف ');</script>";

                    return RedirectToAction("GeneralConditionList");


                }
                else
                {
                    TempData["msg"] = "<script>alert('لم يتم الحذف ');</script>";

                    return RedirectToAction("GeneralConditionList");

                }
            }
            catch
            {
                TempData["msg"] = "<script>alert(' هذا العنصر مرتبط بجول اخر ولا يمكن الحذف ');</script>";
                return RedirectToAction("GeneralConditionList");

            }
        }
    }
}