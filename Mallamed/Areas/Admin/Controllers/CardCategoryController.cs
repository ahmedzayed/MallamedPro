﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize (Roles = "Admin")]
    public class CardCategoryController : Controller
    {
        // GET: Admin/CardCategory
        public ActionResult Index()
        {
            return View();
        }



        public ActionResult CategoryList()
        {
            var model = "SP_GetAllCardCategory".ExecuParamsSqlOrStored(false).AsList<CardCategoryFM>();
            return View(model);

        }
        [HttpGet]
        public ActionResult Create()
        {
            CardCategoryFM obj = new CardCategoryFM();
            return PartialView("~/Areas/Admin/Views/CardCategory/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = "SP_SelectCardCategoryByIds".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<CardCategoryFM>();
            return PartialView("~/Areas/Admin/Views/cardCategory/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(CardCategoryFM model)
        {

            var data = "SP_AddCardCategory".ExecuParamsSqlOrStored(false, "name".KVP(model.Name),"StartCode".KVP(model.StartCode),"EndCode".KVP(model.EndCode),"Value".KVP(model.Value), "Code".KVP(model.Code), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                TempData["msg"] = "<script>alert(' تم الاضافه بنجاح');</script>";

                return RedirectToAction("CategoryList");


            }

            return RedirectToAction("CategoryList");
        }

        public ActionResult Delete(int Id)
        {
            try
            {
                var data = "SP_DeleteCardCategory".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
                if (data != 0)
                {
                    TempData["msg"] = "<script>alert(' تم الحذف');</script>";

                    return RedirectToAction("CategoryList");


                }
                return RedirectToAction("CategoryList");
            }
            catch
            {
                TempData["msg"] = "<script>alert(' هذا العنصر مرتبط بجول اخر ولا يمكن الحذف ');</script>";

                return RedirectToAction("CategoryList");
            }

        }
    }
}