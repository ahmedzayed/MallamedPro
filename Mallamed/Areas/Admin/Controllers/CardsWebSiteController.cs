﻿using Core.Helpers;
using Core.Viewmodels.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mallamed.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class CardsWebSiteController : Controller
    {
        // GET: Admin/CardsWebSite
        public ActionResult Index()
        {
            return View();
        }



        public ActionResult CardWebsiteList()
        {
            var model = "SP_GetAllCardWebsite".ExecuParamsSqlOrStored(false).AsList<CardWebsiteVM>();
            var model1 = model.Where(a => a.Main == false);
            return View(model1.ToList());

        }
        [HttpGet]
        public ActionResult Create()
        {
            CardWebsiteVM obj = new CardWebsiteVM();
            return PartialView("~/Areas/Admin/Views/CardsWebsite/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = "SP_SelectCardWebsiteByIds".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<CardWebsiteVM>();
            return PartialView("~/Areas/Admin/Views/CardsWebsite/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(CardWebsiteVM model)
        {

            var data = "SP_AddCardwebsite".ExecuParamsSqlOrStored(false, "name".KVP(model.CardText), "Id".KVP(model.ID)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("CardWebsiteList");


            }

            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            var data = "SP_DeleteCardwebsite".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return RedirectToAction("CardWebsiteList");


            }
            return Json(new { message = "Erro" });


        }
    }
}