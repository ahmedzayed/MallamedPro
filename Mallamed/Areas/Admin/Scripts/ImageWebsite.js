﻿function Create() {
    var Url = "../ImageWebSite/Create";
    $('#con-close-modal').load(Url, function (response, status, xhr) {
        $('#con-close-modal').modal('show');
    });
}

function Edit(id) {
    var Url = "../ImageWebSite/Edit?Id=" + id;
    $('#con-close-modal').load(Url, function (response, status, xhr) {
        $('#con-close-modal').modal('show');
    });
}


function Save() {
    var $form = $("#ModelForm");
    //if ($form.valid()) {
    var data = new FormData();
    $form.find('input[type="file"]').each(function (count, fileInput) {
        var files = $(fileInput).get(0).files;
        // Add the uploaded image content to the form data collection
        if (files.length > 0) {
            $.each(files, function (index, file) {
                data.append($(fileInput).attr("name") + "[" + index + "]", file);
            });
        }
    });
    var formData = $form.serializeArray();
    $.each(formData, function (key, value) {
        data.append(this.name, this.value);
    });

    //if (IsValid()) {
    ajaxRequest($("#ModelForm").attr('method'), $("#ModelForm").attr('action'), data, 'json', false, false).done(function (result) {

        var res = result.split(',');
        alert("sss");

        //debugger;
        if (res[0] == "success") {
            $('#con-close-modal').modal('hide');
            // window.location = "../ManageNews/Index";
            toastr.success(res[1]);
            Search();
        }
        else
            toastr.error(res[1]);
    });
    //  }
    //}
}