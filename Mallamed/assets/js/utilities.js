$(document).ready(function () {



});
function GetTreeData() {
    var html_content = '';
    var first_level = [];
    var second_level = [];
    $.ajax({
        url: '../json/treeData.json',
        dataType: 'json',
        success: function (data) {
            for (i = 0; i < data.Records.length; i++) {
                var first_level = [];
                for (ii = 0; ii < data.Records[i].child.length; ii++) {
                    first_level.push('<div class="childTree" id="' + data.Records[i].child[ii].ID + '"><div class="childTree-content"><h3>' + data.Records[i].child[ii].Title + '</h3><figure><img src="../assets/images/arab.png" alt="arabian" /></figure><a href="#0" class="socialIcon person"></a><a href="#0" class="socialIcon phone"></a><a href="#0" class="socialIcon whatsApp"></a><a href="#0" class="socialIcon insta"></a><a href="#0" class="socialIcon twitter"></a><a href="#0" class="socialIcon snap"></a></div></div>');
                }
                html_content += '<div class="parentTree" id="' + data.Records[i].ID + '"><div class="parentTree-content"><h3>' + data.Records[i].Title + '</h3><figure><img src="../assets/images/arab.png" alt="arabian" /></figure><a href="#0" class="socialIcon person"></a><a href="#0" class="socialIcon phone"></a><a href="#0" class="socialIcon whatsApp"></a><a href="#0" class="socialIcon insta"></a><a href="#0" class="socialIcon twitter"></a><a href="#0" class="socialIcon snap"></a></div>' + first_level.join("") + '</div>';
            }
            $("#tree").html(html_content);
        },
        complete: function () {
            $('.childTree-content').mouseover(function () {
                $('.childTree-content').find('.socialIcon').removeClass('socialIconOpen');
                $('.parentTree-content').find('.socialIcon').removeClass('socialIconOpen');
                $(this).find('.socialIcon').addClass('socialIconOpen');
            });
            $('.parentTree-content').mouseover(function () {
                $(this).find('.socialIcon').addClass('socialIconOpen');
                $('.childTree-content').find('.socialIcon').removeClass('socialIconOpen');
            });
        },
    });
};

GetTreeData();