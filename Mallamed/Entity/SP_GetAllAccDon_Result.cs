//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Mallamed.Entity
{
    using System;
    
    public partial class SP_GetAllAccDon_Result
    {
        public int ID { get; set; }
        public Nullable<System.DateTime> ContractDate { get; set; }
        public string OrgName { get; set; }
        public Nullable<int> CatID { get; set; }
        public Nullable<System.DateTime> Versiondate { get; set; }
        public string OwnName { get; set; }
        public string ResponsableName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string BookName { get; set; }
        public string TradeActivity { get; set; }
        public Nullable<System.DateTime> finishDate { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Telephone { get; set; }
        public string LocatLang { get; set; }
        public string LocatLat { get; set; }
        public Nullable<int> CreatedUser { get; set; }
        public Nullable<int> IsActive { get; set; }
        public string Photo { get; set; }
        public string Username { get; set; }
        public string Peroid { get; set; }
        public Nullable<System.DateTime> VisiteDate { get; set; }
        public Nullable<System.DateTime> DiscountDate { get; set; }
        public Nullable<int> RegionId { get; set; }
        public Nullable<System.DateTime> ComeOn { get; set; }
        public Nullable<System.DateTime> LeaveOn { get; set; }
        public Nullable<int> CityId { get; set; }
    }
}
