//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Mallamed.Entity
{
    using System;
    
    public partial class SP_GetAllEventSocilamedia_Result
    {
        public int Id { get; set; }
        public string Link { get; set; }
        public string type { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Event { get; set; }
    }
}
