//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Mallamed.Entity
{
    using System;
    
    public partial class SP_GetAllLandPhoto_Result
    {
        public int Id { get; set; }
        public string PhotoPath { get; set; }
        public Nullable<bool> IsSuggestion { get; set; }
        public string Cards { get; set; }
        public string Landmark { get; set; }
    }
}
