﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Mallamed.Startup))]
namespace Mallamed
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
