﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mallamed
{
    public class AppConstants
    {
        #region Images Paths

       
        public const string ImgDefaultEvent = "Default.jpg";
        public const string ImgEvent = "~/Uploads/";

        public const string ImgDefaultDonator = "Default.png";
        public const string ImgDonator = "~/Uploads/";

        public const string ImgDefaultLandMark = "Default.jpg";
        public const string VideoDefaultLandMark = "Default.mp4";
        public const string ImgLandMark = "~/Uploads/";


        public const string ImgDefaultSlider = "s1.jpg";
        public const string ImgSlider = "~/Uploads/";

        public const string ImgDefaultNews = "Default.jpg";
        public const string ImgNews = "~/Uploads/";

        public const string ImgDefaultPartner = "Default.jpg";
        public const string ImgPartner = "~/Uploads/";

        #endregion
    }
}