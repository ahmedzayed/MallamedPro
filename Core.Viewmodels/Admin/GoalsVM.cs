﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Viewmodels.Admin
{
  public  class GoalsVM
    {
        public int Id { get; set; }
        public string Details { get; set; }
        public bool Main { get; set; }
    }
}
