﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Viewmodels.Admin
{
   public class CardCategoryFM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int StartCode { get; set; }
        public int EndCode { get; set; }
        public decimal Value { get; set; }
        public string Code { get; set; }
    }
}
