﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Viewmodels.Admin
{
  public  class LandmarkPhonesFM
    {
        public int Id { get; set; }
        public string PhoneNumber { get; set; }
        public int LandmarkId { get; set; }
    }
}
