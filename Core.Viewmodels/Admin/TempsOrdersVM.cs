﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Viewmodels.Admin
{
   public class TempsOrdersVM
    {
        public string Notes { get; set; }
        public string DonatorsOrg { get; set; }
        public string Donatorsown { get; set; }
        public string DonatorsRe { get; set; }
        public string Service { get; set; }
    }
}
