﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Viewmodels.Admin
{
  public  class LandMArkPhotoFm
    {
        public int Id { get; set; }
        public string PhotoPath { get; set; }
        public bool IsSuggestion { get; set; }
        public int LandmarkId { get; set; }
        public int CardId { get; set; }
    }
}
