﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Viewmodels.Admin
{
   public class FireBaseData
    {
        public string ShortDesc { get; set; }
        public string Description { get; set; }
    }
}
