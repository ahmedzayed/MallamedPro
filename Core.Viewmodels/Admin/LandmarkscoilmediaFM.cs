﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Viewmodels.Admin
{
   public class LandmarkscoilmediaFM
    {
        public int Id { get; set; }
        public int LandmarkId { get; set; }
        public string Link { get; set; }
        public string type { get; set; }
        public bool IsActive { get; set; }
    }
}
