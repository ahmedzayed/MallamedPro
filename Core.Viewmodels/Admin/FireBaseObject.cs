﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Viewmodels.Admin
{
   public class FireBaseObject
    {
        public string to { get; set; }
        public FireBaseData data { get; set; }
        public FireBaseNotfication notification { get; set; }

        public string sound { get; set; }
    }
}
