﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Viewmodels.Admin
{
   public class CardWebsiteVM
    {
        public int ID { get; set; }
        public string CardText { get; set; }
        public bool Main { get; set; }
    }
}
