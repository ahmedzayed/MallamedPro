﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Viewmodels.Admin
{
   public class LandmarkPhonesVM
    {
        public int Id { get; set; }
        public string LandMark { get; set; }
        public string PhoneNumber { get; set; }
    }
}
