﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Viewmodels.Admin
{
  public  class DonatorsVM
    {
        public int ID { get; set; }
        public string Categoires { get; set; }
        public string OrgName { get; set; }
        public string OwnName { get; set; }
        public string ResponsableName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string BookName { get; set; }
        public string Phone1 { get; set; }
        public string Photo { get; set; }
        public string Region { get; set; }
    }
}
