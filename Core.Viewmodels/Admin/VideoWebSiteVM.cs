﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Viewmodels.Admin
{
  public  class VideoWebSiteVM
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string VideoPath { get; set; }
        public bool Isactive { get; set; }
    }
}
