﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Viewmodels.Admin
{
  public  class DonatorsFM
    {
        public int ID { get; set; }
        public DateTime ContractDate { get; set; }
        public string OrgName { get; set; }
        public int CatID { get; set; }
        public DateTime Versiondate { get; set; }
        public string OwnName { get; set; }
        public string ResponsableName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string BookName { get; set; }
        public string TradeActivity { get; set; }
        public DateTime finishDate { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Telephone { get; set; }
        public int RegionId { get; set; }
        public DateTime VisiteDate { get; set; }
        public DateTime DiscountDate { get; set; }
        public int CityId { get; set; }

    }
}
