﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Viewmodels.Admin
{
   public class LandmarkTimeFM
    {
        public int Id { get; set; }
        public string ValidTimeTo { get; set; }
        public string ValidTimeFrom { get; set; }
        public string ValidDayFrom { get; set; }
        public string ValidDayTo { get; set; }
        public int LandmarkId { get; set; }
    }
}
