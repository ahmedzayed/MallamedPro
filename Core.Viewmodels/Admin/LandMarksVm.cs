﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Viewmodels.Admin
{
   public class LandMarksVm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public string Email { get; set; }
        public string LongX { get; set; }
        public string LatX { get; set; }
        public string Region { get; set; }
        public string Landmark { get; set; }
        public string VideoPath { get; set; }
        public string PhotoPath { get; set; }
        public string AudioPath { get; set; }
        public string CopyRight { get; set; }
        public string Owner { get; set; }
        public string CityName { get; set; }
        public string Reference  { get; set; }


    }
}
