﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Viewmodels.Admin
{
   public class SocialWebSiteVM
    {
        public int ID { get; set; }
        public string Link { get; set; }
        public string type { get; set; }
        public bool Isactive { get; set; }
    }
}
