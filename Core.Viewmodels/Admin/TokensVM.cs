﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Viewmodels.Admin
{
  public  class TokensVM
    {
        public int Id { get; set; }
        public string Token { get; set; }
    }
}
