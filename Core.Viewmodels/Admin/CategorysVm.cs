﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Viewmodels.Admin
{
  public  class CategorysVm
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Photo { get; set; }
        public string Icon { get; set; }
    }
}
